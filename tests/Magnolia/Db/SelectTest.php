<?php declare(strict_types=1);

use Magnolia\Db\Select;
use PHPUnit\Framework\TestCase;

final class SelectTest extends TestCase
{
    public function testCanCreateLikeInFilter()
    {
        $select = new Select();

        $select
            ->from('TEST_TABLE')
            ->columns('test_column')
            ->where('test_column', ['%x%', '%y%'], 'LIKE IN');

        $this->assertEquals('SELECT `test_column` FROM `TEST_TABLE` WHERE (`test_column` LIKE \'%x%\' OR `test_column` LIKE \'%y%\')', $select->getSql());
    }
}