### 4.3.0 (2020-04-05)

  * The Table::insertIgnore method has been introduced

### 4.2.0 (2020-03-27)

  * The LIKE IN filter has been introduced. 

### 4.1.0 (2020-02-18)

  * The Cache and Collections\Collection classes have been
    introduced

### 4.0.0 (2019-04-29)

  * Initial release