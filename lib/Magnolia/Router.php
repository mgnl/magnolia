<?php
namespace Magnolia;

class Router
{

    /**
     * @var array
     */
    protected $routes = [];

    /**
     * @param array $routes
     *
     * @return \Magnolia\Router
     */
    public function setRoutes(array $routes = []): Router
    {
        $this->routes = $routes;

        return $this;
    }

    /**
     * @param string $url
     *
     * @return array|bool
     */
    public function resolve($url)
    {
        $find = trim($this->removeBaseUrl($url), '/');

        $parts = explode('/', $find);

        while (count($parts)) {

            $findUrl = implode('/', $parts);

            foreach ($this->routes as $route) {

                $url = trim($route['url']);

                if ($findUrl == $url) {
                    return $route;
                }
            }

            array_pop($parts);
        }

        return false;
    }

    /**
     * @param string $url
     *
     * @return string
     */
    private function removeBaseUrl($url)
    {
        $baseUrl = rtrim(str_replace('\\', '/', dirname($_SERVER['SCRIPT_NAME'])), '/');

        return '/'.trim(preg_replace('~^'.$baseUrl.'~', '', $url), '\\/');
    }
}
