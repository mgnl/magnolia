<?php
namespace Magnolia;

use Magnolia\Registry;

abstract class MagnoliaAbstract
{

    /**
     * Registry handler.
     *
     * @var Registry
     */
    protected $registry = null;

    /**
     * Retrieves object from registry.
     *
     * @param string $name
     * @return mixed
     */
    public function __get($name)
    {
        return $this->registry->get($name);
    }

    /**
     * Sets object into registry.
     *
     * @param string $name
     * @param mixed $value
     * @return void
     */
    public function __set($name, $value)
    {
        $this->registry->set($name, $value);
    }

    /**
     * Sets Registry handler.
     *
     * @return void
     */
    final public function __construct()
    {
        $this->registry = Registry::getInstance();

        call_user_func_array([$this, 'init'], func_get_args());
    }

    /**
     * Initialization method instead __construct();
     *
     * @return void
     */
    public function init()
    {
        
    }
}
