<?php
namespace Magnolia;

use Exception;
use Magnolia\Helper\ArrayHelper;

class View
{

    /**
     * Array of View parameters.
     *
     * @var array
     */
    protected $_params = [
        'template'     => false,
        'content'      => false,
        'templatePath' => false,
    ];

    /**
     * Retrieves object parameter.
     *
     * @param string $name
     * @param mixed $default
     * @return mixed
     */
    public function getParam($name, $default = null)
    {
        if (isset($this->_params[$name])) {
            return $this->_params[$name];
        }

        return $default;
    }

    /**
     * Sets object parameter.
     *
     * @param string $name
     * @param mixed $value
     * @return View
     */
    public function setParam($name, $value)
    {
        $this->_params[$name] = $value;

        return $this;
    }

    /**
     * Renders content from file.
     *
     * @deprecated will be replaced by @see \Core\View::embed($path, $params)
     * @param string $path absolute path to file with contents
     * @return string
     */
    public function getHTML($path, $params = [])
    {
        if (!file_exists($path)) {
            throw new Exception('There is no content.');
        }

        ob_start();

        if (!empty($params)) {
            foreach ($params as $key => $param) {
                $$key = $param;
            }
        }

        include $path;

        if (!empty($params)) {
            foreach ($params as $key => $param) {
                unset($$key);
            }
        }

        return ob_get_clean();
    }

    /**
     * Sets file path into template.
     *
     * @param string $filepath
     * @return View
     */
    public function setTemplate($filepath)
    {
        if (!(bool) $filepath) {
            $this->setParam('templatePath', null);

            return $this;
        }

        if (!is_readable($filepath) || !is_file($filepath)) {
            throw new Exception("Can not read template file: '{$filepath}'.");
        }

        $this->setParam('templatePath', (string) $filepath);

        return $this;
    }

    /**
     * Renders template file.
     *
     * @param bool $filter
     * @return string
     */
    public function render($filter = true)
    {
        if (!$this->getParam('templatePath')) {
            return '';
        }

        extract((array) $this);

        ob_start();

        include $this->getParam('templatePath');

        $this->setParam('content', ob_get_clean());

        return $filter ? $this->filter() : $this->getParam('content');
    }

    /**
     * Renders template from string.
     *
     * @param bool $filter
     * @return string
     */
    public function renderContent($content, $filter = true)
    {
        $this->setParam('content', $content);

        return $filter ? $this->filter() : $this->getParam('content');
    }

    /**
     * Sets template file path.
     *
     * @param string $path
     * @return void
     */
    public function __construct($path = null)
    {
        if (null !== $path) {
            $this->setTemplate($path);
        }
    }

    /**
     * Apply filter on to rendered content.
     *
     * @return	string
     */
    protected function filter()
    {
        $pattern = [];

        $data = ArrayHelper::arrayFromObj($this);

        foreach ($data as $name => $value) {
            if ($value instanceof View) {
                $pattern['#\{\$'.$name.'\}#'] = $value->render();
            } else if (is_scalar($value)) {
                $pattern['#\{\$'.$name.'\}#'] = (string) $value;
            }
        }

        // Clears empty filter variables.
        $pattern['#\{\$.*?\}[\n]*#'] = '';

        return preg_replace(array_keys($pattern), $pattern, $this->getParam('content'));
    }

    /**
     * Assign given properties to the object.
     *
     * @param array $properties
     * @return View
     */
    public function assign($properties)
    {
        foreach ($properties as $name => $value) {
            $this->$name = $value;
        }

        return $this;
    }
}
