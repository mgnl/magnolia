<?php
namespace Magnolia\Helper;

trait Arrays
{

    /**
     * Merges arrays recursively.
     *
     * @return array
     */
    public function arrayMerge()
    {
        $arrays = func_get_args();
        $base   = array_shift($arrays);

        foreach ($arrays as $array) {

            reset($base);

            while (list($key, $value) = each($array)) {

                if (is_array($value) && isset($base[$key]) && is_array($base[$key])) {

                    $base[$key] = $this->arrayMerge($base[$key], $value);
                } else {

                    $base[$key] = $value;
                }
            }
        }

        return $base;
    }

    /**
     * Retrieves array from object (possible recursion).
     *
     * @param &object $object
     * @param bool    $recursive
     *
     * @return array
     */
    public function arrayFromObject(&$object, $recursive = false)
    {
        $retval = [];
        if (is_object($object) || is_array($object)) {
            foreach ($object as $key => $value) {
                if (is_object($value) || is_array($value)) {
                    $retval[$key] = $recursive ? $this->arrayFromObject($value, $recursive) : $value;
                } else {
                    $retval[$key] = $value;
                }
            }
        }

        return $retval;
    }

    /**
     * Extracts keys value from an array.
     *
     * @param array  $array
     * @param string $key
     *
     * @return array
     */
    public function arrayExtractKey(&$array, $key)
    {
        $retval = [];

        foreach ($array as $item) {
            if (isset($item[$key])) {
                $retval[] = $item[$key];
            }
        }

        return $retval;
    }

    /**
     * Replace keys in array.
     *
     * @param array  $array
     * @param string $oldKey
     * @param string $newKey
     *
     * @return array
     */
    public function arrayChangeKey(array $array, string $oldKey, string $newKey): array
    {
        if (!array_key_exists($oldKey, $array)) {
            return $array;
        }

        $keys = array_keys($array);

        $keys[array_search($oldKey, $keys)] = $newKey;

        return array_combine($keys, $array);
    }

    /**
     * Creates array keys from theirs values.
     *
     * @param array $array
     *
     * @return array
     */
    public function arrayValueToKey($array)
    {
        $retval = [];

        foreach ($array as $value) {
            $retval[$value] = $value;
        }

        return $retval;
    }

    /**
     * Converts field value into the array key.
     *
     * @param string $field
     * @param array  $array
     *
     * @return array
     */
    public function arraySubKeyValueToMainKey($field, $array)
    {
        if (!is_array($array)) {
            return [];
        }

        $retval = [];

        foreach ($array as &$subArray) {

            $key = $subArray[$field];

            $retval[$key] = $subArray;
        }

        return $retval;
    }

    /**
     * Extracts recursively the differences from two arrays.
     *
     * @param array $array1
     * @param array $array2
     *
     * @return array
     */
    public function arrayExtractDiff(array &$array1, array &$array2)
    {
        $retval = [];

        foreach ($array1 as $key => &$value) {

            if (array_key_exists($key, $array2)) {

                if (is_array($value) && is_array($array2[$key])) {

                    $retvalR = self::extractDiff($value, $array2[$key]);

                    if (count($retvalR)) {
                        $retval[$key] = $retvalR;
                    }
                } else {
                    if ($value != $array2[$key]) {
                        $retval[$key] = $value;
                    }
                }
            } else {
                $retval[$key] = $value;
            }
        }

        return $retval;
    }

    /**
     * Removes from the array elements with keys not included in $keys.
     *
     * @param array $array
     * @param array $keys
     *
     * @return void
     */
    public function arrayLeftKeys(array &$array, array $keys)
    {
        foreach ($array as $key => &$_) {
            if (!in_array($key, $keys)) {
                unset($array[$key]);
            }
        }
    }

    /**
     * Returns the differences occurring in both tables.
     *
     * @param array $a
     * @param array $b
     *
     * @return array
     */
    public function arrayDiffUnion(array $a = null, array $b = null)
    {
        if (!is_array($a) || !is_array($b)) {
            return null;
        }

        return array_merge(array_diff($a, $b), array_diff($b, $a));
    }
}
