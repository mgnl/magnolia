<?php
namespace Magnolia\Helper;

trait Strings
{

    public function dashToCamelCase(string $input): string
    {
        $item = str_replace(['/', '\\'], "\t", $input);

        $retval = ucwords(str_replace('-', ' ', $item));

        return lcfirst(str_replace([' ', "\t"], ['', '/'], $retval));
    }
}
