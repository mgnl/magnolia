<?php
namespace Magnolia;

class Registry
{

    /**
     * Singleton instance of registry.
     *
     * @var Registry
     */
    protected static $instance = null;

    /**
     * Collection of registered objects.
     *
     * @var array
     */
    protected $registry = [];

    /**
     * Retrieves instance of Registry.
     *
     * @return Registry
     */
    public static function &getInstance()
    {
        if (null === self::$instance) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    /**
     * Returns object from registry or $default value.
     *
     * @param string $name
     * @param mixed $default
     * @return mixed
     */
    public function get($name, $default = null)
    {
        if ($this->has($name)) {
            return $this->registry[$name];
        }

        return $default;
    }

    /**
     * Checks whether the object is set in the registry.
     *
     * @param string $name
     * @return bool
     */
    public function has($name)
    {
        return array_key_exists($name, $this->registry);
    }

    /**
     * Sets the object into the registry collection.
     *
     * @param string $name
     * @param mixed $value
     * @return Registry
     */
    public function set($name, $value)
    {
        $this->registry[$name] = $value;

        return $this;
    }

    /**
     * Singleton model.
     */
    protected function __construct()
    {
        
    }

    /**
     * Singleton model.
     */
    protected function __clone()
    {
        
    }
}
