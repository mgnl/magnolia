<?php

namespace Magnolia\Collections;

use Iterator;

class Collection implements Iterator
{
    /**
     * @var int
     */
    private $position = 0;

    /**
     * @var array
     */
    private $array = [];

    /**
     * Collection constructor.
     *
     * @param array $array
     */
    public function __construct(array $array)
    {
        $this->position = 0;
        $this->array = $array;
    }

    /**
     * @param array|null $fields
     *
     * @return Collection
     */
    public function fields(?array $fields)
    {
        if (!$fields) {

            return new self($this->array);
        }

        $array = [];

        $emptyRecord = array_fill_keys($fields, null);

        foreach ($this->array as $record) {
            $array[] = array_intersect_key($record, $emptyRecord);
        }

        return new self($array);
    }

    /**
     * @param array|null $filters
     *
     * @return Collection
     */
    public function filter(?array $filters)
    {
        if (!$filters) {

            return new self($this->array);
        }

        $array = array_filter($this->array, function ($record) use ($filters) {

            foreach ($filters as $filter) {

                if (!$this->checkFilter($record[$filter[0]], $filter)) {

                    return false;
                }
            }

            return true;
        });

        return new self($array);
    }

    /**
     * @param array|null $order
     *
     * @return Collection
     */
    public function order(?array $order)
    {
        if (!$order) {

            return new self($this->array);
        }

        $array = $this->array;

        if (1 === count($order)) {
            $this->orderByOneFiled($array, $order);
        } else {
            $this->orderByManyFields($array, $order);
        }

        return new self($array);
    }

    /**
     * @param int|null $limit
     * @param int      $offset
     *
     * @return Collection
     */
    public function limit(?int $limit, int $offset = 0)
    {
        if (!$limit) {

            return new self($this->array);
        }

        return new self(array_slice($this->array, (int)$offset, (int)$limit));
    }

    /**
     * @return array
     */
    public function toArray()
    {
        return $this->array;
    }

    /**
     * @inheritDoc
     */
    public function current()
    {
        return $this->array[$this->position];
    }

    /**
     * @inheritDoc
     */
    public function next()
    {
        return ++$this->position;
    }

    /**
     * @inheritDoc
     */
    public function key()
    {
        return $this->position;
    }

    /**
     * @inheritDoc
     */
    public function valid()
    {
        return array_key_exists($this->position, $this->array);
    }

    /**
     * @inheritDoc
     */
    public function rewind()
    {
        return $this->position = 0;
    }

    private function checkFilter($value, array $filter)
    {
        switch ($filter[2]) {
            case '=':
                return $value === $filter[1];
            case '!=':
            case '<>':
                return $value !== $filter[1];
            case '>':
                return $value > $filter[1];
            case '>=':
                return $value >= $filter[1];
            case '<':
                return $value < $filter[1];
            case '<=':
                return $value <= $filter[1];
            case 'BETWEEN':
                return $value >= $filter[1][0] && $value <= $filter[1][0];
            case 'NOT BETWEEN':
                return $value < $filter[1][0] || $value > $filter[1][0];
            case 'IN':
                return in_array($value, $filter[1]);
            case 'NOT IN':
                return !in_array($value, $filter[1]);
            case 'IS NULL':
                return null === $value;
            case 'IS NOT NULL':
                return null !== $value;

            // todo: to implement LIKE and REGEXP

            default:
                return true;
        }
    }

    private function orderByManyFields(array &$array, array $order): void
    {
        foreach ($order as $field => $direction) {
            $toSort[] = array_column($array, $field);
            $toSort[] = constant('SORT_' . strtoupper($direction));
        }

        $toSort[] = &$array;

        array_multisort(... $toSort);
    }

    private function orderByOneFiled(array &$array, array $order): void
    {
        usort($array, function ($a, $b) use ($order) {

            if ('DESC' === strtoupper(reset($order))) {

                return strcmp($b[key($order)], $a[key($order)]);
            }

            return strcmp($a[key($order)], $b[key($order)]);
        });
    }
}