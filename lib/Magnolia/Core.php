<?php
namespace Magnolia;

use Magnolia\Router\Dispatcher;
use Magnolia\Http\Request;
use Magnolia\Http\Response;
use Magnolia\Registry;

class Core
{

    /**
     * Instance of dispatcher.
     *
     * @var Dispatcher
     */
    protected $dispatcher = null;

    /**
     * Instance of registry.
     *
     * @var Registry
     */
    protected $registry = null;

    /**
     * Instance of HTTP request.
     *
     * @var Request
     */
    protected $request = null;

    /**
     * Instance of HTTP response.
     *
     * @var Response
     */
    protected $response = null;

    /**
     * Flag for throwing direct exceptions.
     *
     * @var bool
     */
    public $throwExceptions = false;

    /**
     * Returns instance of dispatcher.
     *
     * @return Dispatcher
     */
    public function getDispatcher()
    {
        if (null === $this->dispatcher) {
            $this->dispatcher = new Dispatcher();
        }

        return $this->dispatcher;
    }

    /**
     * Sets instance of dispatcher.
     *
     * @param Dispatcher $dispatcher
     *
     * @return Application
     */
    public function setDispatcher($dispatcher)
    {
        $this->dispatcher = $dispatcher;

        return $this;
    }

    /**
     * Returns instance of registry.
     *
     * @return Registry
     */
    public function getRegistry()
    {
        if (null === $this->registry) {
            $this->registry = Registry::getInstance();
        }

        return $this->registry;
    }

    /**
     * Sets instance of registry.
     *
     * @param Registry
     *
     * @return Application
     */
    public function setRegistry($registry)
    {
        $this->registry = $registry;

        return $this;
    }

    /**
     * Returns instance of HTTP request.
     *
     * @return Request
     */
    public function getRequest()
    {
        if (null === $this->request) {
            $this->request = new Request();
        }

        return $this->request;
    }

    /**
     * Sets instance of HTTP request.
     *
     * @param Request
     * @return Application
     */
    public function setRequest($request)
    {
        $this->request = $request;

        return $this;
    }

    /**
     * Returns instance of HTTP response.
     *
     * @return Response
     */
    public function getResponse()
    {
        if (null === $this->response) {
            $this->response = new Response();
        }

        return $this->response;
    }

    /**
     * Sets instance of HTTP response.
     *
     * @param Response $response
     * @return Application
     */
    public function setResponse($response)
    {
        $this->response = $response;

        return $this;
    }

    /**
     * Executes request dispositions.
     *
     * @param string $uri
     * @param array $denyControllersList
     *
     * @return Application
     */
    public function run($uri = null)
    {
        if (null === $uri) {
            $uri = $this->getRequest()->getPath();
        }

        $dispatcher = $this->getDispatcher();

        if (!$dispatcher->dispatch($uri)) {
            throw new \Exception("The router could not resolve URI ($uri)");
        }

        $class = $dispatcher->getController();

        if (!$class) {
            throw new \Exception("URI: '{$uri}' does not refer to the controller");
        }

        $action = $dispatcher->getAction();

        if (!$action) {
            throw new \Exception("The action is undefined in the route definition");
        }

        $controller = new $class();

        if (!is_callable([$controller, $action])) {
            throw new \Exception("There is no method '{$action}' in the controller '{$class}'");
        }

        $result = call_user_func_array([$controller, $action], []);

        $this->getResponse()->setOutput($result);

        return $this;
    }

    /**
     * Sends into standard output response.
     *
     * @param bool $render
     *
     * @return string
     */
    public function output($render = true)
    {
        return $this->getResponse()->send($render);
    }

    /**
     * Parses console arguments.
     *
     * @param array $argv
     *
     * @return array
     */
    public static function parseArgv($argv)
    {
        $retval = [];
        foreach ($argv as $param) {
            list($key, $value) = explode('=', $param);
            $retval[$key] = $value;
        }

        return $retval;
    }
}
