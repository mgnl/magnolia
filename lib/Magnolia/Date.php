<?php
namespace Magnolia;

class Date
{

    /**
     * Converts dates timezone to another.
     *
     * @param string $date
     * @param string $tz1
     * @param string $tz2
     * @return string
     */
    public static function convert($date, $tz1, $tz2)
    {
        if (!$date || '0000-00-00 00:00:00' == $date) {
            return null;
        }

        $stz = date_default_timezone_get();

        date_default_timezone_set($tz1);

        $time = strtotime($date);

        date_default_timezone_set($tz2);

        $converted = date('Y-m-d H:i:s', $time);

        date_default_timezone_set($stz);

        return $converted;
    }

    /**
     * Returns date in given timezone.
     *
     * @param string $format
     * @param string $tz
     * @param int $time
     */
    public static function get($format, $tz, $time = null)
    {
        if (null === $time) {
            $time = microtime();
        }

        $stz = date_default_timezone_get();

        date_default_timezone_set($tz);

        $retval = date($format, $time);

        date_default_timezone_set($stz);

        return $retval;
    }
}
