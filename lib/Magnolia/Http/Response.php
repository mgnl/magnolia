<?php
namespace Magnolia\Http;

class Response
{

    /**
     * HTTP response headers.
     *
     * @var array
     */
    protected $headers = [];

    /**
     * The level of compression.
     *
     * @var int
     */
    protected $level = 0;

    /**
     * The body response.
     *
     * @var	string
     */
    protected $output = '';

    /**
     * Adds a header to the response.
     *
     * @param string $header
     * @return Response
     */
    public function addHeader($header)
    {
        $this->headers[] = $header;

        return $this;
    }

    /**
     * Sets the compression level. Possible values: 0-9.
     *
     * @param int $level
     * @return Response
     */
    public function setCompression($level = 5)
    {
        $this->level = $level;

        return $this;
    }

    /**
     * Sets the content of the response.
     *
     * @param string $output
     * @return Response
     */
    public function setOutput($output)
    {
        $this->output = $output;

        return $this;
    }

    /**
     * Sends the response to the HTTP client.
     *
     * @return void
     */
    public function send($render = true)
    {
        if ($this->output) {

            if ($this->level) {
                $output = $this->compress($this->output, $this->level);
            } else {
                $output = $this->output;
            }

            if (!headers_sent()) {
                foreach ($this->headers as $header) {
                    header($header, true);
                }
            }

            if ($render) {
                echo $output;
            }

            return $output;
        }
    }

    /**
     * Redirects to another URL.
     *
     * @param string $url
     * @param int $code
     * @return void
     */
    public function redirect($url, $code = 302)
    {
        header('Status: '.(int) $code);
        header('Location: '.str_replace(['&amp;', "\n", "\r"], ['&', '', ''], $url));
        exit();
    }

    /**
     * Sends the response in JSON format.
     *
     * @param mixed $data
     * @return void
     */
    public static function json($data)
    {
        header('Content-Type: text/javascript; charset=utf8');
        print json_encode($data);
        exit();
    }

    /**
     * Compresses the body of the response if possible.
     *
     * @param string $data
     * @param int $level
     * @return string
     */
    protected function compress($data, $level = 0)
    {
        if (isset($_SERVER['HTTP_ACCEPT_ENCODING']) && (strpos($_SERVER['HTTP_ACCEPT_ENCODING'], 'gzip') !== false)) {
            $encoding = 'gzip';
        }

        if (isset($_SERVER['HTTP_ACCEPT_ENCODING']) && (strpos($_SERVER['HTTP_ACCEPT_ENCODING'], 'x-gzip') !== false)) {
            $encoding = 'x-gzip';
        }

        if (!isset($encoding)) {
            return $data;
        }

        if (!extension_loaded('zlib') || ini_get('zlib.output_compression')) {
            return $data;
        }

        if (headers_sent()) {
            return $data;
        }

        if (connection_status()) {
            return $data;
        }

        $this->addHeader('Content-Encoding: '.$encoding);

        return gzencode($data, (int) $level);
    }
}
