<?php

namespace Magnolia\Http;

class Request
{
    /**
     * The base path in the requested URL.
     *
     * @var string
     */
    protected $baseUrl = null;

    /**
     * The path retrieved from requested URL without $baseUrl.
     *
     * @var string
     */
    protected $path = null;

    /**
     * The parameters set in the request path.
     *
     * @var array
     */
    protected $params = [];

    /**
     * Arguments passed via command line.
     *
     * @var array
     */
    protected static $args = [];

    /**
     * Resolves and returns the base URL from the requested URL.
     *
     * @return string
     */
    public function getBaseUrl()
    {
        if (null === $this->baseUrl) {
            $this->baseUrl = urldecode(rtrim(str_replace('\\', '/', dirname($_SERVER['SCRIPT_NAME'])), '/'));
        }

        return $this->baseUrl;
    }

    /**
     * Retrieves the element from _COOKIE array or returns the default value.
     *
     * @param string $name
     * @param mixed $default
     * @return mixed
     */
    public function getCookie($name, $default = null)
    {
        return isset($_COOKIE[$name]) ? $_COOKIE[$name] : $default;
    }

    /**
     * Retrieves data from _POST or _GET and merged it with _FILES if it is set up.
     *
     * @return array
     */
    public function getData()
    {
        if ($this->isPost()) {
            $data = $_POST;
        } else {
            $data = $_GET;
        }

        if (isset($_FILES)) {
            if (is_array($data)) {
                $data = array_merge($data, $_FILES);
            } else {
                $data = $_FILES;
            }
        }

        return $data;
    }

    /**
     * Retrieves HTTP header.
     *
     * @param string $key
     * @param string $default
     * @return string
     */
    public function getHttpHeader($key = null, $default = null)
    {
        if (null === $key) {
            return apache_request_headers();
        }

        $headers = array_change_key_case(apache_request_headers(), CASE_LOWER);

        $header = strtolower($key);

        return (isset($headers[$header])) ? $headers[$header] : $default;
    }

    /**
     * Retrieves value from route parameters.
     *
     * @param string $name
     * @param mixed $default
     * @return mixed
     */
    public function getParam($name = null, $default = null)
    {
        if (null === $name) {
            return $this->params;
        }

        if (isset($this->params[(string) $name])) {
            return $this->params[(string) $name];
        }

        return $default;
    }

    /**
     * Returns requested path without or with $baseUrl.
     *
     * @param bool $withBaseUrl
     * @return string
     */
    public function getPath($withBaseUrl = false)
    {
        $baseUrl = $this->getBaseUrl();

        if (null === $this->path) {

            $requestUri = $this->getRequestUri();

            $this->path = urldecode(trim(parse_url($requestUri, PHP_URL_PATH), '/'));

            if (!empty($baseUrl)) {
                $esc        = chr(27);
                $this->path = trim(preg_replace($esc.'^'.$baseUrl.$esc, '', '/'.$this->path), '/');
            }

            $this->path = '/'.$this->path;
        }

        if ($withBaseUrl) {

            if ('/' != $this->path) {

                return $baseUrl.$this->path;
            }

            return $baseUrl;
        }

        return $this->path;
    }

    public function getMethod()
    {
        return $_SERVER['REQUEST_METHOD'];
    }

    /**
     * Returns value from _POST or default value.
     *
     * @param string $name
     * @param mixed $default
     * @return mixed
     */
    public function getPost($name, $default = null)
    {
        return isset($_POST[$name]) ? $_POST[$name] : $default;
    }

    /**
     * Returns value from _GET or default value.
     *
     * @param string $name
     * @param mixed $default
     * @return mixed
     */
    public function getQuery($name, $default = null)
    {
        return isset($_GET[$name]) ? $_GET[$name] : $default;
    }

    /**
     * Returns raw posted body.
     *
     * @param mixed $default
     * @return string|mixed
     */
    public function getRawPost($default = null)
    {
        if ($this->isPost()) {
            return file_get_contents('php://input');
        }

        return $default;
    }

    /**
     * Returns value from _REQUEST or default value.
     *
     * @param string $name
     * @param mixed $default
     * @return mixed
     */
    public function getRequest($name, $default = null)
    {
        return isset($_REQUEST[$name]) ? $_REQUEST[$name] : $default;
    }

    /**
     * Returns requested URI.
     *
     * @param mixed $default
     * @return string|mixed
     */
    public function getRequestUri($default = null)
    {
        if (!isset($_SERVER['REQUEST_URI'])) {
            $_SERVER['REQUEST_URI'] = substr($_SERVER['PHP_SELF'], 1);

            if (isset($_SERVER['QUERY_STRING'])) {
                $_SERVER['REQUEST_URI'] .= '?'.$_SERVER['QUERY_STRING'];
            }
        }

        return $this->getServer('REQUEST_URI', $default);
    }

    /**
     * Returns value from _SERVER or default value.
     *
     * @param string $name
     * @param mixed $default
     * @return mixed
     */
    public function getServer($name, $default = null)
    {
        return isset($_SERVER[$name]) ? $_SERVER[$name] : $default;
    }

    /**
     * Returns value from _SESSION or default value.
     *
     * @param string $name
     * @param mixed $default
     * @return mixed
     */
    public function getSession($name, $default = null)
    {
        return isset($_SESSION[$name]) ? $_SESSION[$name] : $default;
    }

    /**
     * Checks the request is CLI.
     *
     * @return bool
     */
    public function isCli()
    {
        return php_sapi_name() === 'cli';
    }

    /**
     * Checks the requested method is Get.
     *
     * @return bool
     */
    public function isGet()
    {
        return 'GET' === $this->getServer('REQUEST_METHOD');
    }

    /**
     * Checks the request is json.
     *
     * @return bool
     */
    public function isJson()
    {
        $accept = $this->getHttpHeader('Accept');

        return false !== strpos($accept, 'application/json');
    }

    /**
     * Checks the request is JsonP.
     *
     * @return bool
     */
    public function isJsonP()
    {
        if ($this->isJson()) {
            return false;
        }

        $accept = $this->getHttpHeader('Accept');

        return false !== strpos($accept, 'application/javascript');
    }

    /**
     * Checks the requested method is POST.
     *
     * @return bool
     */
    public function isPost()
    {
        return 'POST' === $this->getServer('REQUEST_METHOD');
    }

    /**
     * Checks whether the connection is secure or normal.
     *
     * @return bool
     */
    public function isSsl()
    {
        $header = $this->getServer('HTTPS');

        return in_array($header, [1, '1', 'on', 'On']);
    }

    /**
     * Checks whether the connection is via AJAX or not.
     *
     * @return bool
     */
    public function isXhr()
    {
        return 'xmlhttprequest' === strtolower($this->getServer('HTTP_X_REQUESTED_WITH'));
    }

    /**
     * Sets parameters.
     *
     * @param string|array $spec
     * @param mixed $value
     * @return void
     */
    public function setParam($spec, $value = null)
    {
        if (null === $value) {
            if (is_array($spec) || is_object($spec)) {
                foreach ((array) $spec as $name => $value) {
                    self::setParam($name, $value);
                }
            } else {
                unset($this->params[(string) $spec]);
            }
        } else {
            $this->params[(string) $spec] = $value;
        }
    }

    /**
     * Clears the parameters of the request.
     *
     * @return $this
     */
    public function resetParams()
    {
        $this->params = [];

        return $this;
    }

    /**
     * Retrieves client IP.
     *
     * @return string
     */
    public function getIp()
    {
        if (($ip = $this->getServer('HTTP_X_REAL_IP', null))) {
            return $ip;
        }

        return $this->getServer('REMOTE_ADDR', null);
    }

    /**
     * Retrieves command line argument's value.
     *
     * @param string $name
     * @param mixed $default
     * @return mixed
     */
    public function getArg($name, $default = null)
    {
        if (isset(self::$args[$name])) {
            return self::$args[$name];
        }

        return $default;
    }

    /**
     * Sets command line arguments.
     *
     * @param array $argv
     * @return array
     */
    public static function setArgs($argv)
    {
        foreach ($argv as $param) {
            list($key, $value) = explode('=', $param);
            self::$args[$key] = $value;
        }

        return self::$args;
    }
}
