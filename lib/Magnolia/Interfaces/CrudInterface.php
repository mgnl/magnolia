<?php

namespace Magnolia\Interfaces;

interface CrudInterface
{

    /**
     * Creates the resource or resources in the storage
     *
     * @param array  $resources
     * @param string $containerName
     * @param bool   $isCollection
     *
     * @return int|bool
     */
    public function create($resources, $containerName = null, $isCollection = false);

    /**
     * Retrieves resources from the storage
     *
     * @param array $criteria
     * @param array $order
     * @param array $fields
     * @param int   $limit
     * @param int   $offset
     *
     * @return array
     */
    public function retrieve(
        array $criteria = [],
        array $order = [],
        array $fields = [],
        int $limit = 0,
        int $offset = 0
    ): array;

    /**
     * Updates resources in the storage
     *
     * @param array  $resources
     * @param array  $criteria
     * @param string $containerName
     *
     * @return bool
     */
    public function update($resources, $criteria, $containerName = null);

    /**
     * Deletes resources from the storage
     *
     * @param array  $criteria
     * @param string $containerName
     *
     * @return bool
     */
    public function delete(array $criteria, $containerName = null);
}
