<?php
namespace Magnolia\Encryption;

class OpenSsl
{

    /**
     * The key with which the data will be encrypted/decrypted.
     *
     * @var string
     */
    protected $key = null;

    /**
     * Vector used for the initialization in CBC mode.
     *
     * @var string
     */
    protected $iv = null;

    /**
     * SSL encryption method.
     *
     * @var string
     */
    protected $sslMethod = 'aes-256-cfb';

    /**
     * Sets key and initialization vector data.
     *
     * @param string $key
     * @param string $iv
     *
     * @return void
     */
    public function setKeyAndIv($key, $iv)
    {
        $this->key = $key;
        $this->iv  = $iv;
    }

    /**
     * Sets SSL encryption/decryption method.
     *
     * @param string $method
     *
     * @return this
     */
    public function setSslMethod($method)
    {
        $this->sslMethod = $method;

        return $this;
    }

    /**
     * Class constructor - sets key and vector data.
     *
     * @param string $key
     * @param string $iv
     *
     * @return void
     */
    public function __construct($key = null, $iv = null, $sslMethod = null)
    {
        if ($key && $iv) {
            $this->setKeyAndIv($key, $iv);
        }

        if ($sslMethod) {
            $this->setSslMethod($sslMethod);
        }
    }

    /**
     * Encrypts to OpenSSL's data.
     *
     * @param string $string
     *
     * @return string
     */
    public function encrypt($string)
    {
        return openssl_encrypt($string, $this->sslMethod, hex2bin($this->key), 0, hex2bin($this->iv));
    }

    /**
     * Decrypts OpenSSL's data.
     *
     * @param string $encryptedString
     *
     * @return string
     */
    public function decrypt($encryptedString)
    {
        return openssl_decrypt($encryptedString, $this->sslMethod, hex2bin($this->key), 0, hex2bin($this->iv));
    }
}
