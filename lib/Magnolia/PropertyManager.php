<?php
namespace Magnolia;

class PropertyManager
{

    /**
     * The properties collection - multidimentional array.
     *
     * @var array
     */
    protected $properties = [];

    /**
     * The pointer to the desired key in the properties array - separated
     * by chr(27).
     *
     * @var string
     */
    protected $pointer = null;

    /**
     * Sets the pointer on the desired key.
     *
     * @param string $name
     *
     * @return $this
     */
    public function __get($name)
    {
        if (null === $this->pointer) {
            $this->pointer = $name;
        } else {
            $this->pointer .= chr(27).$name;
        }

        return $this;
    }

    /**
     * Returns the value of the property from the key indicated in
     * the $pointer property.
     *
     * @param string $name
     * @param mixed $default Returned default value
     *
     * @return mixed
     */
    public function __call($name, $default)
    {
        $pointers = explode(chr(27), $this->pointer);

        $this->pointer = null;

        $properties = &$this->properties;

        foreach ($pointers as $pointer) {
            if (isset($properties[$pointer])) {
                $properties = &$properties[$pointer];
            } elseif (isset($default[0])) {
                return $default[0];
            }
        }

        if (isset($properties[$name])) {
            return $properties[$name];
        } elseif (isset($default[0])) {
            return $default[0];
        }
    }

    /**
     * Sets properties array.
     *
     * @param array $properties
     *
     * @return void
     */
    public function __construct(array $properties = [])
    {
        $this->properties = $properties;
    }

    /**
     * Sets properties array.
     *
     * @param array $properties
     *
     * @return $this
     */
    public function setProperties(array $properties = [])
    {
        $this->properties = $properties;

        return $this;
    }

    /**
     * Returns properties array.
     *
     * @return array
     */
    public function getProperties()
    {
        return $this->properties;
    }
}
