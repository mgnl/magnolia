<?php
namespace Magnolia\Router;

use Magnolia\Router;

class Dispatcher
{

    protected $router;
    protected $controller;
    protected $action;
    protected $params;

    public function getRouter()
    {
        if (null === $this->router) {
            $this->router = new Router();
        }

        return $this->router;
    }

    public function setRouter($router)
    {
        $this->router = $router;

        return $this;
    }

    public function getController()
    {
        return $this->controller;
    }

    public function getAction()
    {
        return $this->action;
    }

    public function getParams()
    {
        return [];
    }

    public function dispatch($url)
    {
        $route = $this->getRouter()->resolve($url);

        if (!$route) {
            $route = $this->resolveDirect($url);
        }

        if (!$route) {
            return false;
        }

        if (isset($route['controller'])) {

            $parts = explode('::', $route['controller']);

            $this->controller = $parts[0];

            $this->action = $parts[1];

            return true;
        }

        return false;
    }

    private function resolveDirect($uri)
    {
        $path = ucwords(str_replace('-', ' ', str_replace(['/', '\\'], "\t", $uri)));

        $class = str_replace([' ', "\t"], ['', '/'], $path);

        $parts = explode('/', $class);

        $action = 'index';

        do {

            $file = 'Controller/'.implode('/', $parts).'Controller';

            if (stream_resolve_include_path($file.'.php')) {

                $class = str_replace('/', '\\', $file);

                return [
                    'controller' => "{$class}::{$action}Action",
                ];
            }

            $action = lcfirst(array_pop($parts));
        } while ($action);
    }
}
