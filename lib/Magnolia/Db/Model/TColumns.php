<?php
namespace Magnolia\Db\Model;

use Magnolia\Db\RawSql;

trait TColumns
{

    /**
     * Prepares columns for SELECT statement.
     *
     * @param array $requestedColumns
     *
     * @return array
     */
    public function prepareColumns($requestedColumns = [])
    {
        if (empty($requestedColumns)) {
            $requestedColumns = array_keys($this->columns);
        }

        $retval = [];
        foreach ($requestedColumns as $column) {
            if (isset($this->columns[$column])) {

                $columnType = isset($this->columns[$column]['type']) ? $this->columns[$column]['type'] : null;

                if ($columnType == 'raw_sql') {
                    #TODO: throw if 'sql' is not set.
                    $retval[$column] = new RawSql($this->columns[$column]['sql'].' AS '.$this->db->fieldize($column));
                } elseif ($columnType == 'alias') {
                    #TODO: throw if 'sql' is not set.
                    $retval[$column] = $this->db->fieldize($this->columns[$column]['sql']).' as '.$this->db->fieldize($column);
                } else {
                    if (isset($this->columns[$column]['sql'])) {
                        $retval[$column] = $this->columns[$column]['sql'];
                    } else {
                        $retval[$column] = $column;
                    }
                }
            } else {
                // throw
            }
        }

        return $retval;
    }

    /**
     * Prepares columns for WHERE and HAVING statements.
     *
     * @param array $filters
     *
     * @return array
     */
    public function prepareFilters($filters)
    {
        $retval['where']  = [];
        $retval['having'] = [];

        foreach ($filters as $filter) {

            if (isset($this->columns[$filter[0]])) {

                $type = $this->columns[$filter[0]]['type'] ?? null;

                if ($type && 'raw_sql' == $type) {

                    $retval['having'][] = $filter;
                } else {

                    if (isset($this->columns[$filter[0]]['sql'])) {
                        $filter[0] = $this->columns[$filter[0]]['sql'];
                    }

                    $retval['where'][] = $filter;
                }
            }
        }

        return $retval;
    }
    /**
     * columns definition example:
     *
     * $columns = [
     *      'id' => [
     *          'type' => 'raw_sql | alias | int | string | bool | datetime',
     *          'sql'  => ''
     *      ],
     * ];
     */
}
