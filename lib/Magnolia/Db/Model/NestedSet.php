<?php
namespace Magnolia\Db\Model;

use Magnolia\Db\RawSql;

abstract class NestedSet extends Table
{

    /**
     * Left index field name.
     *
     * @var	string
     */
    protected $lft = 'lft';

    /**
     * Right index field name.
     *
     * @var	string
     */
    protected $rgt = 'rgt';

    /**
     * Level number field name.
     *
     * @var	string
     */
    protected $level = 'level';

    /**
     * Parent id field name.
     *
     * @var string
     */
    protected $parent = 'parent_id';

    /**
     * Retrieves one record.
     *
     * @param int $recordId
     * @return array|bool
     */
    public function fetch($recordId)
    {
        return $this->getSelect(true)
                        ->where($this->primary, $recordId)
                        ->fetch();
    }

    /**
     * Retrieves records from rootId to given level.
     *
     * @param int $rootId
     * @param int $level
     * @return array|bool
     */
    public function fetchTree($rootId = null, $level = null)
    {
        if (null === $rootId) {
            $this->getSelect(true)
                    ->order($this->lft);

            if (is_int($level)) {
                $this->select->where($this->level, $level + 1, '<=');
            }

            return $this->fetchAll();
        }

        $between = [new RawSql('p.'.$this->lft), new RawSql('p.'.$this->rgt)];

        $this->getSelect($this->name.' n, '.$this->name.' p')
                ->columns('n.*')
                ->where('n.'.$this->lft, $between, 'BETWEEN')
                ->where('p.'.$this->primary, (int) $rootId)
                ->order('n.'.$this->lft);

        if (is_int($level)) {
            $this->select->where('n.'.$this->level, new RawSql('p.'.$this->level.'+'.$level), '<=');
        }

        return $this->fetchAll();
    }

    /**
     * Retrieves nodes in the path to the given node.
     *
     * @param int $nodeId
     * @return array
     */
    public function fetchPath($nodeId)
    {
        $between = [new RawSql('p.'.$this->lft), new RawSql('p.'.$this->rgt)];

        return $this->getSelect($this->name.' n, '.$this->name.' p')
                        ->columns('p.*')
                        ->where('n.'.$this->lft, $between, 'BETWEEN')
                        ->where('n.'.$this->primary, $nodeId)
                        ->order('p.'.$this->lft)
                        ->fetch(true);
    }

    /**
     * Retrieves nodes on given level.
     *
     * @param int $level
     * @return array|bool
     */
    public function fetchDepth($level = 1)
    {
        return $this->getSelect(true)
                        ->where($this->level, (int) $level)
                        ->order($this->lft)
                        ->fetch(true);
    }

    /**
     * Inserts node into the tree.
     *
     * @param array $data
     * @param string $type      'append'|'prepend'
     * @return int|bool
     */
    public function insertNode($data, $parentId = null, $type = 'append')
    {
        switch ($type) {
            case 'append': return $this->appendNode($data, (int) $parentId);
            case 'prepend': return $this->prependNode($data, (int) $parentId);
        }

        return false;
    }

    /**
     * Inserts node into the tree as last child.
     *
     * @param array $data
     * @param int $parentId
     * @return int|bool
     */
    public function appendNode($data, $parentId = null)
    {
        $depth = 0;

        if ($parentId && is_int($parentId)) {
            $parent = $this->fetch($parentId);

            if (!$parent) {
                return false;
            }

            $rgt   = $parent[$this->rgt];
            $depth = $parent[$this->level] + 1;
        } else {
            $rgt = $this->getMaxRight() + 1;
        }

        $sql_lft = 'UPDATE '.$this->name.' SET '.$this->lft.' = '.$this->lft.' + 2 WHERE '.$this->lft.' > '.$rgt;
        $sql_rgt = 'UPDATE '.$this->name.' SET '.$this->rgt.' = '.$this->rgt.' + 2 WHERE '.$this->rgt.' >= '.$rgt;

        if ($this->db->query($sql_lft)) {
            if ($this->db->query($sql_rgt)) {
                $data[$this->lft]    = $rgt;
                $data[$this->rgt]    = $rgt + 1;
                $data[$this->level]  = $depth;
                $data[$this->parent] = $parentId;

                return $this->insert($data);
            }
        }

        return false;
    }

    /**
     * Inserts node into the tree as first child.
     *
     * @param array $data
     * @param int $parentId
     * @return int|bool
     */
    public function prependNode($data, $parentId = null)
    {
        $level = 0;

        if ($parentId && is_int($parentId)) {
            $parent = $this->fetch($parentId);

            if (!$parent) {
                return false;
            }

            $lft   = $parent[$this->lft] + 1;
            $level = $parent[$this->level] + 1;
        } else {
            $lft = 1;
        }

        $sql_lft = 'UPDATE '.$this->name.' SET '.$this->lft.' = '.$this->lft.' + 2 WHERE '.$this->lft.' >= '.$lft;
        $sql_rgt = 'UPDATE '.$this->name.' SET '.$this->rgt.' = '.$this->rgt.' + 2 WHERE '.$this->rgt.' >= '.$lft;

        if ($this->db->query($sql_lft)) {
            if ($this->db->query($sql_rgt)) {
                $data[$this->lft]    = $lft;
                $data[$this->rgt]    = $lft + 1;
                $data[$this->level]  = $level;
                $data[$this->parent] = $parentId;

                return $this->insert($data);
            }
        }

        return false;
    }

    /**
     * Deletes branch from given node.
     *
     * @param int $id
     * @return	bool
     */
    public function deleteNode($id)
    {
        if (!$id) {
            return false;
        }

        $node = $this->fetch($id);

        if (!$node) {
            return false;
        }

        $size = ( $node[$this->rgt] - $node[$this->lft] ) + 1;
        $sql  = 'DELETE FROM '.$this->name.' WHERE '.$this->lft.' BETWEEN '.$node[$this->lft].' AND '.$node[$this->rgt];

        $this->db->query($sql);

        return $this->compressSpace($size, $node[$this->rgt]);
    }

    /**
     * Converts flat (left-right) array into nested PHP array.
     *
     * @param array $nodes
     * @return array
     */
    public function getNestedNodes($nodes)
    {
        $levels = [];
        $i      = 0;

        foreach ($nodes as $node) {

            if (0 == $node['parent_id']) {

                unset($node['parent_id'], $node['lft'], $node['rgt'], $node['level']);

                $levels[$i] = $node;

                $levels[$i]['children'] = $this->getChildren($node['id'], $nodes);

                $i++;
            }
        }

        return $levels;
    }

    /**
     * Retrieves children of node.
     *
     * @param int $id
     * @param array $nodes
     * @return array
     */
    public function getChildren($id, $nodes)
    {
        $retval = [];
        $i      = 0;

        foreach ($nodes as $nodeId => $node) {

            if ($id == $node['parent_id']) {

                unset($node['parent_id'], $node['lft'], $node['rgt'], $node['level']);

                $retval[$i] = $node;

                unset($nodes[$nodeId]);

                $retval[$i]['children'] = $this->getChildren($node['id'], $nodes);

                $i++;
            }
        }

        return $retval;
    }

    /**
     * Retrieves highest value of left index.
     *
     * @return	int|bool
     */
    protected function getMaxRight()
    {
        $stmt = $this->db->query('SELECT MAX('.$this->rgt.') AS `max_right` FROM '.$this->name);

        if ($stmt && $stmt->numRows) {
            return $stmt->row['max_right'];
        }

        return false;
    }

    /**
     * Removes space resulting from deleting or moving records.
     *
     * @param int $size
     * @param int $rgt
     * @return bool
     */
    protected function compressSpace($size, $rgt)
    {
        $lft_sql = 'UPDATE '.$this->name.' SET '.$this->lft.' = '.$this->lft.' - '.$size.' WHERE '.$this->lft.' > '.(int) $rgt;
        $rgt_sql = 'UPDATE '.$this->name.' SET '.$this->rgt.' = '.$this->rgt.' - '.$size.' WHERE '.$this->rgt.' > '.(int) $rgt;

        if ($this->db->query($lft_sql)) {
            return $this->db->query($rgt_sql);
        }

        return false;
    }
}
