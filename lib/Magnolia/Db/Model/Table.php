<?php

namespace Magnolia\Db\Model;

use Magnolia\Db\Adapter;
use Magnolia\Db\RawSql;
use Magnolia\Db\Select;
use Magnolia\Interfaces\CrudInterface;

abstract class Table implements CrudInterface
{
    /**
     * Global database adapter handler.
     *
     * @var Adapter
     */
    public static $dbAdapter = null;

    /**
     * Table fields.
     *
     * @var array
     */
    protected $fields = [];

    /**
     * Table name.
     *
     * @var string
     */
    protected $name = null;

    /**
     * Primary key field name.
     *
     * @var string
     */
    protected $primary = null;

    /**
     * Instance of select object.
     *
     * @var Select
     */
    protected $select = null;

    /**
     * Local database adapter handler.
     *
     * @var Adapter
     */
    protected $db = null;

    /**
     * @var int
     */
    protected $total = null;

    /**
     * Returns table fields list.
     *
     * @return array
     */
    public function getFields()
    {
        return $this->fields;
    }

    /**
     * Sets table fields list.
     *
     * @param array $fields
     *
     * @return Table
     */
    public function setFields(array $fields)
    {
        $this->fields = $fields;

        return $this;
    }

    /**
     * Returns table name.
     *
     * @return string
     */
    public function getTable($sanitized = false, $cutAlias = false)
    {
        $name = $this->name;

        if ($cutAlias) {
            $table = explode(' ', $this->name);
            $name  = $table[0];
        }

        return $sanitized ? $this->db->fieldize($name) : $name;
    }

    /**
     * Sets table name.
     *
     * @param string $tableName
     *
     * @return Table
     */
    public function setTable($tableName)
    {
        $this->name = (string)$tableName;

        return $this;
    }

    /**
     * Returns primary key field name.
     *
     * @return string
     */
    public function getPrimaryKey($sanitized = false)
    {
        return $sanitized ? $this->db->fieldize($this->primary) : $this->primary;
    }

    /**
     * Sets primary key field name.
     *
     * @param string $primaryKeyName
     *
     * @return Table
     */
    public function setPrimaryKey($primaryKeyName)
    {
        $this->primary = (string)$primaryKeyName;

        return $this;
    }

    /**
     * Returns instance of select object.
     *
     * @param string $from
     *
     * @return Select
     */
    public function getSelect($from = null)
    {
        $this->select->reset();

        if (null !== $from) {
            if (true === $from) {
                $this->select->from($this->name);
            } else {
                $this->select->from($from);
            }
        }

        return $this->select;
    }

    /**
     * Sets instance of select object.
     *
     * @param Select $select
     *
     * @return Table
     */
    public function setSelect(Select $select)
    {
        $this->select = $select;

        return $this;
    }

    /**
     * Returns database adapter handler.
     *
     * @return Adapter
     */
    public function getDbAdapter()
    {
        if (null === $this->db || $this->db !== self::$dbAdapter) {

            $this->db = self::$dbAdapter;
        }

        return $this->db;
    }

    /**
     * Sets the database adapter
     *
     * @param Adapter $adapter
     *
     * @return $this
     */
    public function setDbAdapter(Adapter $adapter)
    {
        $this->db = $adapter;

        return $this;
    }

    /**
     * @return int
     */
    public function getTotal()
    {
        return $this->total;
    }

    /**
     * @param int $total
     *
     * @return $this
     */
    public function setTotal($total = 0)
    {
        $this->total = $total;

        return $this;
    }

    /**
     * Sets instances of select and database adapter objects.
     *
     * @return void
     */
    public function __construct()
    {
        $db           = $this->getDbAdapter();
        $this->select = new Select($db);

        $this->init();
    }

    /**
     * Cause final __construct().
     *
     * @return Table
     */
    public function init()
    {
        return $this;
    }

    /**
     * Inserts new record into database.
     *
     * @param array|object $records
     * @param string       $table
     *
     * @param bool         $collection
     *
     * @return void
     */
    public function create($records, $table = null, $collection = false): void
    {
        $this->insert($records, $table, $collection);
    }

    public function insertIgnore($records, $table = null, $collection = false)
    {
        return $this->insert($records, $table, $collection, true);
    }

    public function insert($records, $table = null, $collection = false, $ignore = false)
    {
        if (!$collection) {
            $records = [$records];
        }

        $table  = null === $table ? $this->getTable(true) : $this->db->fieldize($table);
        $fields = implode(', ', $this->db->fieldize(reset($records)));

        $sql = sprintf(
            "INSERT %s INTO %s(%s)VALUES",
            $ignore ? 'IGNORE' : '',
            $table,
            $fields
        );

        foreach ($records as $record) {
            $insert[] = '(' . implode(', ', $this->db->sanitize($record)) . ')';
        }

        $sql .= implode(', ', $insert);

        $result = $this->db->query($sql);

        if ($result) {

            $id = $this->db->getLastId();

            if (1 == count($records) && 0 !== $id) {
                return $id;
            }

            return true;
        }

        return 0;
    }

    /**
     * Updates database record(s).
     *
     * @param array  $data
     * @param array  $where
     * @param string $table
     *
     * @return int
     */
    public function update($data, $where, $table = null)
    {
        $whereParams = $this->select->getParam('where');

        $this->select->reset('where');

        foreach ($where as $cond) {
            switch (true) {
                case isset($cond[3]):
                    $this->select->where($cond[0], $cond[1], $cond[2], $cond[3]);
                    break;
                case isset($cond[2]):
                    $this->select->where($cond[0], $cond[1], $cond[2]);
                    break;
                case isset($cond[1]):
                    $this->select->where($cond[0], $cond[1]);
                    break;
            }
        }

        $where = $this->select->prepareWhere();

        $this->select->setParam('where', $whereParams);

        $table = null === $table ? $this->getTable(true) : '`' . $table . '`';

        $update = 'UPDATE ' . $table . ' SET ' . $this->db->prepareSet($data) . ' ';
        $update .= $where;

        $result = $this->db->query($update);

        if ($result) {
            return true;
        }

        return false;
    }

    /**
     * Deletes records matches to passed conditions.
     *
     * @param array  $where
     * @param string $table
     *
     * @return bool
     */
    public function delete(array $where, $table = null)
    {
        $whereParams = $this->select->getParam('where');

        $this->select->reset('where');

        foreach ($where as $cond) {
            switch (true) {
                case isset($cond[3]):
                    $this->select->where($cond[0], $cond[1], $cond[2], $cond[3]);
                    break;
                case isset($cond[2]):
                    $this->select->where($cond[0], $cond[1], $cond[2]);
                    break;
                case isset($cond[1]):
                    $this->select->where($cond[0], $cond[1]);
                    break;
            }
        }

        $whereSql = $this->select->prepareWhere();

        $this->select->setParam('where', $whereParams);

        $tableSql = null !== $table ? $this->db->fieldize($table) : $this->getTable(true);

        $sql = 'DELETE FROM ' . $tableSql . $whereSql;

        return $this->db->query($sql);
    }

    /**
     * Fetches single record from database.
     *
     * @return array|bool
     */
    public function fetchOne()
    {
        $from = $this->select->getParam('from');

        if (!$from) {
            $this->select->setParam('from', [$this->name]);
        }

        $stmt = $this->db->query($this->select);

        if ($stmt) {
            if ($stmt->numRows) {
                return $stmt->row;
            }
        }

        return false;
    }

    /**
     * Returns filtered data.
     *
     * @param bool $asStatement
     * @param bool $unbuffer
     *
     * @return array
     */
    public function fetchAll($asStatement = false, $unbuffer = false)
    {
        $fromUnion = $this->select->getParam('from_union');

        if (!$fromUnion) {

            $from = $this->select->getParam('from');

            if (!$from) {
                $this->select->setParam('from', [$this->name]);
            }
        }

        $stmt = $this->db->query($this->select, $asStatement, $unbuffer);

        if ($asStatement) {
            return $stmt;
        }

        if ($stmt) {
            if ($stmt->numRows) {
                return $stmt->rows;
            }
            return [];
        }

        return false;
    }

    /**
     * Returns number of rows.
     *
     * @param string $table
     *
     * @return int|bool
     */
    public function count($table = null)
    {
        if (null == $table) {
            $table = $this->name;
        }

        $result = $this->db->query('SELECT COUNT(*) AS `count` FROM ' . $this->db->fieldize($table));

        if ($result) {
            return $result->row['count'];
        }

        return false;
    }

    /**
     * @return int
     */
    public function affectedRows()
    {
        return $this->db->affectedRows();
    }

    /**
     * @param array $filters
     *
     * @return bool
     */
    public function isUnique(array $filters): bool
    {
        $this->getSelect()
            ->columns(new RawSql("NOT COUNT(*) AS `unique`"))
            ->filter($filters);

        $result = $this->fetchOne();

        if ($result && $result['unique']) {
            return true;
        }

        return false;
    }

    /**
     * @param mixed $filters
     * @param array $order
     * @param array $columns Requested columns
     * @param int   $limit
     * @param int   $offset
     *
     * @return array
     */
    public function retrieve(
        array $filters = [],
        array $order = [],
        array $columns = [],
        int $limit = 0,
        int $offset = 0): array
    {
        $this->getSelect()
            ->columns($columns)
            ->filter($filters)
            ->limit($limit, $offset)
            ->order($order);

        if (isset($this->join) && isset($this->join->table) && isset($this->join->on)) {

            $this->join->on = str_replace('{table}', $this->name, $this->join->on);

            if (isset($this->join->type)) {
                $this->select->join($this->join->table, $this->join->on, $this->join->type);
            } else {
                $this->select->join($this->join->table, $this->join->on);
            }
        }

        $results = $this->fetchAll();

        if ($limit) {
            $this->total = $this->select->count();
        } else {
            $this->total = count($results);
        }

        if ($results) {
            return $results;
        }

        return [];
    }
}
