<?php
namespace Magnolia\Db\Criteria;

use DomainException;
use Magnolia\Db\Adapter\AdapterInterface;

class Filter
{

    /**
     * Available comparision operatos.
     *
     * @see https://dev.mysql.com/doc/refman/8.0/en/comparison-operators.html
     *
     * @const array
     */
    const COMPARISON = [
        'BETWEEN',
        'COALESCE',
        '=',
        '<=>',
        '>',
        '>=',
        'GREATEST',
        'IN',
        'INTERVAL',
        'IS',
        'IS NOT',
        'IS NOT NULL',
        'IS NULL',
        'LEAST',
        '<',
        '<=',
        'LIKE',
        'NOT BETWEEN',
        '!=',
        '<>',
        'NOT IN',
        'NOT LIKE',
        'STRCMP',
        'REGEXP',
        'NOT REGEXP',
    ];

    /**
     * Available logical operators.
     *
     * @see https://dev.mysql.com/doc/refman/8.0/en/logical-operators.html
     *
     * @const array
     */
    const LOGIC = [
        'AND',
        '&&',
        'OR',
        '||',
        'XOR',
        'NOT',
        '!'
    ];

    /**
     * @var AdapterInterface
     */
    private static $db;

    /**
     * @var array
     */
    private static $comparisonGroups = [
        'and'      => ['BETWEEN', 'NOT BETWEEN'],
        'brackets' => ['COALESCE', 'GREATEST', 'IN', 'INTERVAL', 'ISNULL', 'LEAST', 'NOT IN', 'STRCMP'],
    ];

    /**
     * The array of raw filter.
     *
     * @var array
     */
    private $rawFilter = [];

    /**
     * The column name in the table or the name of the data set field.
     * It depends on algorithm of fetching results.
     *
     * @var string
     */
    private $field;

    /**
     * Value(s) with which the $field should be filtered.
     *
     * @var mixed
     */
    private $value;

    /**
     * Comparison operator.
     *
     * @see Filer::COMPARISON
     *
     * @var enum
     */
    private $comparison;

    /**
     * Logical operator.
     *
     * @see Filter::LOGIC
     *
     * @var enum
     */
    private $logic;

    /**
     * Field names with which the filter is to be combined using parentheses.
     *
     * @var array
     */
    private $combineWith = [];

    /**
     * Sets DB adapter.
     *
     * @param AdapterInterface $dbAdapter
     *
     * @return void
     */
    public static function setDbAdapter(AdapterInterface $dbAdapter): void
    {
        self::$db = $dbAdapter;
    }

    /**
     * Normalizes filter array to assoc array with proper fields names.
     * 
     * @param array $filter
     *
     * @return array
     */
    public static function nomalizeFilter(array $filter): array
    {
        $retval['name']       = $filter[0] ?? null;
        $retval['value']      = $filter[1] ?? null;
        $retval['comparison'] = $filter[2] ?? '=';
        $retval['logic']      = $filter[3] ?? 'AND';
        $retval['combine']    = $filter[4] ?? [];

        $retval['comparision'] = strtoupper($retval['comparison']);
        $retval['logic']       = strtoupper($retval['logic']);

        return $retval;
    }

    /**
     * Returns filter name.
     *
     * @return string
     */
    public function getName(): string
    {
        return $this->field;
    }

    /**
     * @param bool $logic
     *
     * @return string
     */
    public function buildSql(bool $logic = true): string
    {
        if (self::$db) {
            $field = self::$db->fieldize($this->field);
            $value = self::$db->sanitize($this->value);
        } else {
            $field = $this->field;
            $value = $this->value;
        }

        $sql = $logic ? ' '.$this->logic.' ' : '';

        $sql .= $field.' ';

        if (in_array(strtoupper($this->comparison), self::$comparisonGroups['and'])) {

            $sql .= $this->comparison.$value[0].' AND '.$value[1];
        } elseif (in_array(strtoupper($this->comparison), self::$comparisonGroups['brackets'])) {

            $sql .= $this->comparison.'('.implode(', ', $value).')';
        } else {

            $sql .= $this->comparison.' '.$value;
        }

        return $sql;
    }

    /**
     * @param array $filter
     */
    final public function __construct(array $filter)
    {
        $this->rawFilter = $filter;

        $this->setField();

        $this->value = $this->rawFilter['value'] ?? null;

        $this->setComparison();
        $this->setLogic();
        $this->setCombineWith();
    }

    /**
     * Sets field name.
     *
     * @return void
     *
     * @throws DomainException
     */
    private function setField(): void
    {
        $this->field = $this->rawFilter['name'] ?? null;

        if (!is_string($this->field)) {
            throw new DomainException(sprintf("Invalid filter field type. It should be a string, '%s' given", gettype($this->field)));
        }
    }

    /**
     * Sets comparison operator.
     *
     * @return void
     *
     * @throws DomainException
     */
    private function setComparison(): void
    {
        $this->comparison = $this->rawFilter['comparison'] ?? null;

        if (!in_array($this->comparison, self::COMPARISON)) {
            throw new DomainException(sprintf("Invalid condition parameter '%s'.", $this->comparison));
        }

        if ('IN' === $this->comparison && is_scalar($this->value)) {
            $this->comparison = '=';
        } elseif ('NOT IN' === $this->comparison && is_scalar($this->value)) {
            $this->comparison = '<>';
        } elseif ('=' === $this->comparison && is_array($this->value)) {
            $this->comparison = 'IN';
        } elseif (in_array($this->comparison, ['<>', '!=']) && is_array($this->value)) {
            $this->comparison = 'NOT IN';
        } elseif ('=' === $this->comparison && null === $this->value) {
            $this->comparison = 'IS NULL';
        } elseif (in_array($this->comparison, ['<>', '!=']) && null === $this->value) {
            $this->comparison = 'IS NOT NULL';
        }

        // @todo: change comparison:
        // consider NULL and other
    }

    /**
     * Sets logical operator.
     * 
     * @return void
     *
     * @throws DomainException
     */
    private function setLogic(): void
    {
        $this->logic = $this->rawFilter['logic'] ?? null;

        if (!in_array($this->logic, self::LOGIC)) {
            throw new DomainException(sprintf("Invalid logical parameter '%s'.", $this->logic));
        }
    }

    /**
     * Sets combine with fields.
     *
     * @return void
     *
     * @throws DomainException
     */
    private function setCombineWith(): void
    {
        $this->combineWith = $this->rawFilter['combine'] ?? [];

        if (!is_array($this->combineWith)) {
            throw new DomainException(sprintf("Invalid combine type. It should be an array, '%s' given", gettype($this->field)));
        }

        foreach ($this->combineWith as $field) {

            if (!is_string($field)) {
                throw new DomainException(sprintf("Invalid combine element type. It should be a string, '%s' given", gettype($this->field)));
            }
        }
    }
}
