<?php
namespace Magnolia\Db\Criteria;

/**
 * The object for filter (WHERE or JOIN statement) managment.
 */
class FiltersManager
{

    /**
     * @var array
     */
    private $filters = [];

    /**
     * @param array $filter
     *
     * @return void
     */
    public function addFilter(array $filter): void
    {
        if (isset($filter['name'])) {
            $this->filters[] = new Filter($filter);
        } else {
            $this->filters[] = new Filter(Filter::nomalizeFilter($filter));
        }
    }

    /**
     * @return string
     */
    public function buildSql(): string
    {
        $i = 0;

        $sql = '';

        foreach ($this->filters as $filter) {

            $sql .= $filter->buildSql($i++);
        }

        return $sql;
    }
}
