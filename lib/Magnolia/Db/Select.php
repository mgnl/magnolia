<?php

namespace Magnolia\Db;

use InvalidArgumentException;
use LogicException;

// todo: Implement filters method
// todo: Clear join
// todo: Implement having method

class Select
{
    /**
     * Available condition operators.
     *
     * @var array
     */
    protected static $conditions = [
        '=',
        '!=',
        '<>',
        '>',
        '>=',
        '<',
        '<=',
        'LIKE',
        'LIKE IN',
        'REGEXP',
        'BETWEEN',
        'NOT BETWEEN',
        'IN',
        'NOT IN',
        'IS NULL',
        'IS NOT NULL',
    ];

    /**
     * Database adapter handler.
     *
     * @var    Adapter
     */
    protected $db = null;

    /**
     * Array of query elements.
     *
     * @var array
     */
    protected $select = [
        'distinct'   => false,
        'columns'    => [],
        'from'       => [],
        'from_union' => [],
        'join'       => [],
        'where'      => [],
        'group_by'   => [],
        'having'     => [],
        'order_by'   => [],
        'offset'     => false,
        'limit'      => false,
    ];

    /**
     * Sets database adapter.
     *
     * @param Adapter $db
     *
     * @return $this
     */
    public function setDbAdapter(Adapter $db)
    {
        $this->db = $db;

        return $this;
    }

    /**
     * Returns database adapter.
     *
     * @return Adapter
     */
    public function getDbAdapter()
    {
        return $this->db;
    }

    /**
     * Returns array of select parameters.
     *
     * @param string $param
     *
     * @return array
     */
    public function getParam($param = null)
    {
        if (null === $param) {
            return $this->select;
        } elseif (isset($this->select[$param])) {
            return $this->select[$param];
        }
    }

    /**
     * Sets select parameter.
     *
     * @param string $param
     * @param mixed  $value
     *
     * @return $this
     */
    public function setParam($param, $value)
    {
        if (isset($this->select[$param])) {
            $this->select[$param] = $value;
        }

        return $this;
    }

    /**
     * Sets query parameters.
     *
     * @param array $data
     *
     * @return $this
     */
    public function setSelect($data)
    {
        if (!count($data)) {
            $this->select = [
                'distinct'   => false,
                'columns'    => [],
                'from'       => [],
                'from_union' => [],
                'join'       => [],
                'where'      => [],
                'group_by'   => [],
                'having'     => [],
                'order_by'   => [],
                'offset'     => false,
                'limit'      => false,
            ];
        } else {
            $this->select = (array)$data;
        }

        return $this;
    }

    /**
     * Clears query elements.
     *
     * @param string|null $param
     *
     * @return $this
     */
    public function reset($param = null)
    {
        if (null === $param) {
            $this->setSelect([]);

            return $this;
        }

        if (isset($this->select[$param])) {
            if (in_array($param, ['distinct', 'limit', 'offset'])) {
                $this->select[$param] = false;
            } else {
                $this->select[$param] = [];
            }
        }

        return $this;
    }

    /**
     * Sets database adapter if passed.
     *
     * @param Adapter $db
     *
     * @return void
     */
    public function __construct(Adapter $db = null)
    {
        if (null !== $db) {
            $this->db = $db;
        }
    }

    /**
     * Executes query and returns results.
     *
     * @return array|bool
     */
    public function fetch($all = false)
    {
        $stmt = $this->getDbAdapter()->query($this->__toString());

        if ($stmt) {
            if (!$stmt->numRows) {
                return [];
            }
            if (1 === $stmt->numRows) {
                if ($all) {
                    return $stmt->rows;
                }

                return $stmt->row;
            }
            if (1 < $stmt->numRows) {
                return $stmt->rows;
            }
        }

        return false;
    }

    /**
     * Sets distinct flag.
     *
     * @param bool $flag
     *
     * @return $this
     */
    public function distinct($flag = true)
    {
        $this->select['distinct'] = (bool)$flag;

        return $this;
    }

    /**
     * Sets columns.
     *
     * @param array|string $columns
     *
     * @return $this
     */
    public function columns($columns = null)
    {
        $this->add($this->select['columns'], $columns);

        return $this;
    }

    /**
     * Sets from tables.
     *
     * @param array|string $from
     *
     * @return $this
     */
    public function from($from = null)
    {
        $this->add($this->select['from'], $from);

        return $this;
    }

    /**
     * @param string $sql
     *
     * @return $this
     */
    public function fromUnion($sql)
    {
        $this->select['from_union'][] = $sql;

        return $this;
    }

    /**
     * Adds join into query parameters.
     *
     * @param string $table
     * @param string $on
     * @param string $type
     *
     * @return $this
     */
    public function join($table, $on, $type = 'LEFT')
    {
        $raw = ($on instanceof RawSql);
        if ($raw) {
            $join = $on;
        } else {
            $join = explode('=', $on, 2);
        }

        $this->select['join'][] = [
            'table'   => $table,
            'on'      => $on,
            'field_1' => $raw ? null : trim($join[0]),
            'field_2' => $raw ? null : trim($join[1]),
            'raw'     => $raw ? $join : null,
            'type'    => $type,
        ];

        return $this;
    }

    /**
     * Adds where conditions.
     *
     * @param string $column
     * @param mixed  $values
     * @param string $condition
     * @param string $logic
     *
     * @return $this
     */
    public function where($column, $values = null, $condition = '=', $logic = 'AND')
    {
        if (!in_array(strtoupper($condition), self::$conditions)) {
            if (!$column instanceof RawSql) {
                throw new InvalidArgumentException("Invalid condition: {$condition}");
            }
        }

        if (!in_array(strtoupper($logic), ['AND', 'OR'])) {
            throw new InvalidArgumentException("Invalid logic operator: {$logic}");
        }

        $this->select['where'][] = [
            'column'    => $column,
            'value'     => $values,
            'condition' => strtoupper($condition),
            'logic'     => strtoupper($logic)
        ];

        return $this;
    }

    /**
     * Adds grouped fields into query parameters.
     *
     * @param array|string $group
     *
     * @return $this
     */
    public function group($group)
    {
        $this->add($this->select['group_by'], $group);

        return $this;
    }

    /**
     * Adds conditions for having statement.
     *
     * @param string $alias
     * @param mixed  $values
     * @param string $condition
     * @param string $logic
     *
     * @return $this
     */
    public function having($alias, $values = null, $condition = '=', $logic = 'AND')
    {
        if (!in_array(strtoupper($condition), self::$conditions)) {
            throw new InvalidArgumentException("Invalid condition: {$condition}");
        }

        if (!in_array(strtoupper($logic), ['AND', 'OR'])) {
            throw new InvalidArgumentException("Invalid logic operator: {$logic}");
        }

        $this->select['having'][] = [
            'alias'     => $alias,
            'value'     => $values,
            'condition' => strtoupper($condition),
            'logic'     => strtoupper($logic)
        ];

        return $this;
    }

    /**
     * Sets order parameters.
     *
     * @param string $column
     * @param string $type
     *
     * @return $this
     */
    public function order($column = null, $type = 'ASC')
    {
        if (is_array($column) && empty($column)) {
            return $this;
        }

        if (null === $column) {
            $this->select['order_by'] = [];

            return $this;
        }

        if (is_array($column)) {
            foreach ($column as $col => $type) {
                $this->select['order_by'][] = ['column' => $col, 'type' => $type];
            }
        } else {
            $this->select['order_by'][] = ['column' => $column, 'type' => $type];
        }

        return $this;
    }

    /**
     * Sets limit and offset.
     *
     * @param int $limit
     * @param int $offset
     *
     * @return $this
     */
    public function limit($limit = 0, $offset = 0)
    {
        $this->select['limit']  = (int)$limit;
        $this->select['offset'] = (int)$offset;

        return $this;
    }

    /**
     * Counts all rows, executes query without any conditions.
     *
     * @return int
     */
    public function count()
    {
        $columns = $this->select['columns'];
        $order   = $this->select['order_by'];
        $limit   = $this->select['limit'];
        $offset  = $this->select['offset'];

        if (!$this->select['distinct']) {
            foreach ($this->select['columns'] as $num => $column) {

                if (!($column instanceof RawSql)) {
                    unset($this->select['columns'][$num]);
                }
            }
        }

        $this->select['order_by'] = [];
        $this->select['limit']    = null;
        $this->select['offset']   = null;

        if (empty($this->select['columns'])) {
            $this->select['columns'][] = new RawSql('COUNT(*)');
        }

        $result = $this->getDbAdapter()->count((string)$this);

        $this->select['columns']  = $columns;
        $this->select['order_by'] = $order;
        $this->select['limit']    = $limit;
        $this->select['offset']   = $offset;

        return $result;
    }

    /**
     * Sets filter into where statement.
     *
     * @param array|null $filter
     *
     * @return $this
     */
    public function filter(?array $filter)
    {
        if (!$filter) {

            return $this;
        }

        foreach ($filter as $cond) {

            $field     = $cond[0] ?? null;
            $values    = $cond[1] ?? null;
            $condition = $cond[2] ?? '=';
            $logic     = $cond[3] ?? 'AND';

            $this->where($field, $values, $condition, $logic);
        }

        return $this;
    }

    /**
     * Sets filter into having statement.
     *
     * @param array $filter
     *
     * @return $this
     */
    public function filterHaving(array $filter)
    {
        foreach ($filter as $cond) {

            $field     = $cond[0] ?? null;
            $values    = $cond[1] ?? null;
            $condition = $cond[2] ?? '=';
            $logic     = $cond[3] ?? 'AND';

            $this->having($field, $values, $condition, $logic);
        }

        return $this;
    }

    /**
     * Converts object parameters into SQL string.
     *
     * @return string
     */
    public function __toString()
    {
        return $this->getSql();
    }

    public function getSql()
    {
        $retval = '';
        $retval .= $this->prepareColumns();
        $retval .= $this->prepareFrom();
        $retval .= $this->prepareFromUnion();
        $retval .= $this->prepareJoin();
        $retval .= $this->prepareWhere();
        $retval .= $this->prepareGroup();
        $retval .= $this->prepareHaving();
        $retval .= $this->prepareOrder();
        $retval .= $this->prepareLimit();

        return $retval;
    }

    /**
     * Prepares columns list.
     *
     * @return string
     */
    protected function prepareColumns()
    {
        $select = "SELECT ";

        $select .= $this->select['distinct'] ? 'DISTINCT ' : '';

        if (count($this->select['columns'])) {
            $db = $this->getDbAdapter();

            $temp = [];

            foreach ($this->select['columns'] as $column) {
                $temp[] = $this->quoteColumn($column);
            }

            $select .= implode(', ', $temp);
        } else {
            $select .= '*';
        }

        return $select;
    }

    /**
     * Prepares tables list.
     *
     * @return string
     */
    protected function prepareFrom()
    {
        if (empty($this->select['from']) && !empty($this->select['from_union'])) {
            return '';
        }

        $from = ' FROM ';
        $temp = [];
        if (count($this->select['from'])) {
            $db = $this->getDbAdapter();

            foreach ($this->select['from'] as $table) {
                $temp[] = $this->quoteColumn($table);
            }
        } else {
            throw new LogicException("No definition of the tables in the FROM clause");
        }

        return $from . implode(', ', array_unique($temp));
    }

    /**
     * @return string
     *
     * @throws LogicException
     */
    protected function prepareFromUnion()
    {
        if (!empty($this->select['from'])) {
            return;
        }

        $from = ' FROM (';

        if (count($this->select['from_union'])) {

            $from .= implode(' UNION ALL ', $this->select['from_union']);
        } else {
            throw new LogicException("There have to be FROM or FROM_UNION settings");
        }

        return $from . ') FROM_UNION';
    }

    /**
     * Prepares joins statements.
     *
     * @return string
     */
    protected function prepareJoin()
    {
        $retval = ' ';

        if (count($this->select['join'])) {
            $db = $this->getDbAdapter();

            foreach ($this->select['join'] as $join) {
                $retval .= strtoupper($join['type']) . ' JOIN ';
                if ($join['raw']) {
                    $retval .= $this->quoteColumn($join['table']) . ' ON ' . $join['raw'] . ' ';
                } else {
                    $retval .= $this->quoteColumn($join['table']) . ' ON(' . $this->quoteColumn($join['field_1']) . ' = ' . $this->quoteColumn($join['field_2']) . ') ';
                }
            }
        }

        return rtrim($retval);
    }

    /**
     * Prepares conditions for where statement.
     *
     * @return string
     */
    public function prepareWhere()
    {
        if (!count($this->select['where'])) {
            return '';
        }

        $retval = ' WHERE ';

        foreach ($this->select['where'] as $key => $where) {

            if ('=' == $where['condition'] && is_array($where['value'])) {
                $where['condition'] = 'IN';
            } elseif ('IN' == $where['condition'] && is_scalar($where['value'])) {
                $where['condition'] = '=';
            }

            if (0 == $key) {
                $retval .= $this->formatCondition($where['column'], $where['value'], $where['condition']);
            } else {
                $retval .= ' ' . $where['logic'] . ' ';
                $retval .= $this->formatCondition($where['column'], $where['value'], $where['condition']);
            }
        }

        return $retval;
    }

    /**
     * Prepares group statement.
     *
     * @return string
     */
    protected function prepareGroup()
    {
        $temp = [];
        $db   = $this->getDbAdapter();
        if (count($this->select['group_by']) > 0) {
            foreach ($this->select['group_by'] as $group) {
                $temp[] = $this->quoteColumn($group);
            }
        }

        return count($temp) ? ' GROUP BY ' . implode(', ', $temp) : '';
    }

    /**
     * Prepare having statement conditions.
     *
     * @return string
     */
    protected function prepareHaving()
    {
        if (!count($this->select['having'])) {
            return '';
        }

        $retval = ' HAVING ';

        foreach ($this->select['having'] as $key => $where) {
            if (0 == $key) {
                $retval .= $this->formatCondition($where['alias'], $where['value'], $where['condition']);
            } else {
                $retval .= ' ' . $where['logic'] . ' ';
                $retval .= $this->formatCondition($where['alias'], $where['value'], $where['condition']);
            }
        }

        return $retval;
    }

    /**
     * Prepares sort parameters.
     *
     * @return string
     */
    protected function prepareOrder()
    {
        $order = [];

        $db = $this->getDbAdapter();
        foreach ($this->select['order_by'] as $params) {
            $order[] = $this->quoteColumn($params['column']) . ' ' . $params['type'];
        }

        return count($order) ? ' ORDER BY ' . implode(', ', $order) : '';
    }

    /**
     * Prepares limit and offset.
     *
     * @return string
     */
    protected function prepareLimit()
    {
        $limit  = (int)$this->select['limit'];
        $offset = (int)$this->select['offset'];

        if ($limit > 0) {
            return ' LIMIT ' . ($offset ? $offset . ', ' : '') . $limit;
        }

        return '';
    }

    /**
     * Adds parameters into the local array.
     *
     * @param &array $localArray
     * @param array|null $array
     *
     * @return $this
     */
    protected function add(&$localArray, $array = null)
    {
        if (null === $array) {
            $localArray = [];

            return $this;
        }

        if (is_string($array)) {
            $array = explode(',', $array);
            foreach ($array as $ind => $value) {
                $array[$ind] = trim($value);
            }
        }

        if ($array instanceof RawSql) {
            $localArray[] = $array;

            return $this;
        }

        $localArray = array_merge($localArray, $array);

        array_unique($localArray);

        return $this;
    }

    /**
     * Formats query condition.
     *
     * @param string $column
     * @param mixed  $values
     * @param string $condition
     *
     * @return string
     */
    public function formatCondition($column, $values, $condition = '=')
    {
        if ($column instanceof RawSql) {
            return $column;
        }

        if (null === $values) {
            if ('=' == $condition) {
                $condition = 'IS NULL';
            } elseif ('!=' == $condition || '<>' == $condition) {
                $condition = 'IS NOT NULL';
            }
        }

        $db = $this->getDbAdapter();

        $column = $this->quoteColumn($column);
        $values = $this->quoteValue($values);

        if (in_array($condition, ['=', '!=', '<>', '>', '>=', '<', '<=', 'LIKE', 'REGEXP'])) {
            #TODO: validate whether every elements is string
            return $column . ' ' . $condition . ' ' . $values;
        } elseif (in_array($condition, ['IN', 'NOT IN'])) {
            return sprintf("%s {$condition}(%s)", $column, implode(', ', (array)$values));
        } elseif (in_array($condition, ['BETWEEN', 'NOT BETWEEN'])) {
            return sprintf("%s {$condition} %s AND %s", $column, $values[0], $values[1]);
        } elseif (in_array($condition, ['IS NULL', 'IS NOT NULL'])) {
            return $column . ' ' . $condition;
        } elseif ('LIKE IN' === $condition) {
            return $this->prepareLikeIn($column, $values);
        } elseif (false === $condition) {
            return $column;
        }

        #TODO throw
    }

    /**
     * @param $column
     * @param $values
     *
     * @return string
     */
    private function prepareLikeIn($column, $values): string
    {
        foreach ($values as $value) {
            $likeIn[] = "$column LIKE $value";
        }

        if (!isset($likeIn)) {
            return '';
        }

        return '(' . implode(' OR ', $likeIn) . ')';
    }

    /**
     * @param      $spec
     * @param bool $useKeys
     *
     * @return array|string
     */
    private function quoteColumn($spec, $useKeys = true)
    {
        if (is_array($spec)) {
            if ($useKeys) {
                $spec = array_keys($spec);
            }

            foreach ($spec as $key => $field) {
                $spec[$key] = $this->quoteColumn($field, $useKeys);
            }

            return $spec;
        }

        if ($spec instanceof RawSql) {
            return $spec;
        }

        $dot   = explode('.', $spec, 2);
        $table = isset($dot[1]) ? $dot[0] . '.' : '';
        $space = isset($dot[1]) ? explode(' ', $dot[1], 2) : explode(' ', $dot[0], 2);
        $alias = isset($space[1]) ? ' ' . $space[1] : '';
        $field = '*' == $space[0] ? '*' : '`' . str_replace('`', '', $space[0]) . '`';

        return $table . $field . $alias;
    }

    /**
     * @param      $spec
     * @param bool $quote
     *
     * @return array|string
     */
    private function quoteValue($spec, $quote = true)
    {
        if (is_array($spec)) {
            foreach ($spec as $field => $value) {
                $spec[$field] = $this->quoteValue($value, $quote);
            }
            return $spec;
        }

        if ($spec instanceof RawSql) {
            return $spec;
        }

        if (null === $spec) {
            return 'NULL'; // This is not quoted string 'NULL' value, in SQL it is just NULL
        }

        if (is_numeric($spec)) {
            return $spec;
        }

        if (0 === strpos($spec, '@')) {
            return $this->quoteColumn(substr($spec, 1), false);
        }

        return $quote ? "'" . addslashes($spec) . "'" : addslashes($spec);
    }
}
