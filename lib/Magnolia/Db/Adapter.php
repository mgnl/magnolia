<?php

namespace Magnolia\Db;

use Exception;
use Magnolia\Db\Adapter\AdapterInterface;
use Magnolia\Db\RawSql;

class Adapter implements AdapterInterface
{

    /**
     * Database adapter handler.
     *
     * @var	AdapterInterface
     */
    protected $adapter = null;

    /**
     * Logger handler.
     *
     * @var Logger
     */
    protected $logger = null;

    /**
     * Returns DB Adapter.
     *
     * @return AdapterInterface
     */
    public function getAdapter()
    {
        return $this->adapter;
    }

    /**
     * Returns Logger handler.
     *
     * @return Logger
     */
    public function getLogger()
    {
        return $this->logger;
    }

    /**
     * Sets Logger handler.
     *
     * @param Logger $logger
     *
     * @return Adapter
     */
    public function setLogger($logger)
    {
        $this->logger = $logger;

        return $this;
    }

    /**
     * Creates instance of database adapter.
     *
     * @param string $adapter
     * @param string $host
     * @param string $user
     * @param string $pass
     * @param string $dbname
     * @param int $port
     *
     * @return void
     *
     * @throw Exception
     */
    public function __construct($adapter, $host, $user, $pass, $dbname = '', $port = 3306)
    {
        if (class_exists($adapter, true)) {
            $this->adapter = new $adapter($host, $user, $pass, $dbname, $port);
        } else {
            throw new Exception("Cannot load database adapter class: {$adapter}.");
        }
    }

    /**
     * Sets current database.
     *
     * @param string $database
     *
     * @return bool
     */
    public function database($database)
    {
        return $this->adapter->database($database);
    }

    /**
     * Returns query result.
     *
     * @param string $sql
     * @param bool $asStatement
     *
     * @return mixed
     */
    public function query(string $sql, bool $asStatement = false, bool $unbuffer = false)
    {
        if ($this->logger) {
            $this->logger->debug($sql);
        }

        $result = $this->adapter->query($sql, $asStatement, $unbuffer);

        return $result;
    }

    /**
     * Sanitize SQL string.
     *
     * @param string $value
     *
     * @return string
     */
    public function escape($value)
    {
        return $this->adapter->escape($value);
    }

    /**
     * Returns affected rows.
     *
     * @return int
     */
    public function affectedRows()
    {
        return $this->adapter->affectedRows();
    }

    /**
     * Returns ID of last inserted row.
     *
     * @return int
     */
    public function getLastId()
    {
        return $this->adapter->getLastId();
    }

    /**
     * Sanitize values.
     *
     * @param string|array $data
     * @param bool $quote
     *
     * @return mixed
     */
    public function sanitize($data, $quote = true)
    {
        if (is_array($data)) {
            foreach ($data as $field => $value) {
                $data[$field] = $this->sanitize($value, $quote);
            }
            return $data;
        }

        if ($data instanceof RawSql) {
            return $data;
        }

        if (null === $data) {
            return 'NULL'; // This is not quoted string 'NULL' value, in SQL it is just NULL
        }

        if (is_numeric($data)) {
            return $data;
        }

        if (0 === strpos($data, '@')) {
            return $this->fieldize(substr($data, 1), false);
        }

        return $quote ? "'".$this->escape($data)."'" : $this->escape($data);
    }

    /**
     * Sanitize fields or tables names.
     *
     * @param string|array $data
     * @param bool $useKeys
     *
     * @return string|array
     */
    public function fieldize($data, $useKeys = true)
    {
        if (is_array($data)) {
            if ($useKeys) {
                $data = array_keys($data);
            }

            foreach ($data as $key => $field) {
                $data[$key] = $this->fieldize($field, $useKeys);
            }

            return $data;
        }

        if ($data instanceof RawSql) {
            return $data;
        }

        $dot   = explode('.', $data, 2);
        $table = isset($dot[1]) ? $dot[0].'.' : '';
        $space = isset($dot[1]) ? explode(' ', $dot[1], 2) : explode(' ', $dot[0], 2);
        $alias = isset($space[1]) ? ' '.$space[1] : '';
        $field = '*' == $space[0] ? '*' : '`'.str_replace('`', '', $space[0]).'`';

        return $table.$field.$alias;
    }

    /**
     * Prepares elements for SET statement.
     *
     * @param array $data
     *
     * @return string
     */
    public function prepareSet($data)
    {
        $retval = '';

        foreach ((array) $data as $field => $value) {
            $retval .= $this->fieldize($field).' = '.$this->sanitize($value).', ';
        }

        return rtrim($retval, ', ');
    }

    /**
     * Starts DB transaction.
     *
     * @return bool
     */
    public function startTransaction()
    {
        $this->query('set autocommit = 0');
        $this->query('start transaction');
    }

    /**
     * Commits DB transaction.
     *
     * @return bool
     */
    public function commit()
    {
        $this->query('commit');
        $this->query('set autocommit = 1');
    }

    /**
     * Rollback DB transaction.
     *
     * @return bool
     */
    public function rollback()
    {
        $this->query('rollback');
        $this->query('set autocommit = 1');
    }

    /**
     * Validates order by direction.
     *
     * @param string $order
     * @param string|null $default
     *
     * @return string|null
     */
    public function validOrder($order, $default = null)
    {
        $order = strtolower($order);

        if (in_array($order, ['asc', 'desc'])) {
            return $order;
        }

        return $default;
    }

    /**
     * Deletes all rows from table and sets AUTO_INCREMENT = 1.
     *
     * @param string $table
     *
     * @return bool
     */
    public function truncate($table)
    {
        $table = $this->fieldize($table);

        if ($this->query('DELETE FROM '.$table)) {
            return $this->query('ALTER TABLE '.$table.' AUTO_INCREMENT = 1');
        }

        return false;
    }

    /**
     * Returns number of rows.
     *
     * @param string $sql
     *
     * @return int
     *
     * @throw Exception
     */
    public function count($sql)
    {
        if ($this->logger) {
            $this->logger->debug($sql);
        }

        return $this->adapter->count($sql);
    }

    /**
     * Retrieves the number of rows obtained in the last query.
     *
     * @return int
     */
    public function numRows()
    {
        return $this->adapter->numRows;
    }
}
