<?php
namespace Magnolia\Db\Adapter;

use Exception;

class MysqliAdapter
{

    /**
     * Connection link.
     *
     * @var Resource
     */
    protected $link = null;

    /**
     * The number of rows obtained in the last query.
     *
     * @var int
     */
    public $numRows = null;

    /**
     * Establish connection.
     *
     * @param string $host
     * @param string $user
     * @param string $pass
     * @param string $dbname
     * @param int $port
     *
     * @return void
     */
    public function __construct($host, $user, $pass, $dbname = '', $port = 3306)
    {
        $this->link = new \mysqli($host, $user, $pass, $dbname, $port);

        if ($this->link->connect_errno) {
            throw new Exception($this->link->connect_error, $this->link->connect_errno);
        }

        $this->link->query("SET NAMES 'utf8'");
        $this->link->query("SET CHARACTER SET utf8");
        $this->link->query("SET CHARACTER_SET_CONNECTION=utf8");
        $this->link->query("SET SQL_MODE = ''");
    }

    /**
     * Executes SQL query.
     *
     * @param string $sql
     * @param bool $asStatement
     *
     * @return stdClass|bool
     */
    public function query($sql, $asStatement = false, $unbuffer = true)
    {
        if ($unbuffer) {
            $resource = $this->link->query($sql, MYSQLI_USE_RESULT);
        } else {
            $resource = $this->link->query($sql);
        }

        if (!$this->link->errno) {
            if ($resource instanceof \mysqli_result) {

                $this->numRows = $resource->num_rows;

                if ($asStatement) {
                    return $resource;
                }

                $i = 0;

                $data = [];

                while ($result = $resource->fetch_assoc()) {
                    $data[$i] = $result;
                    $i++;
                }
                $resource->close();

                $query          = new \stdClass();
                $query->row     = isset($data[0]) ? $data[0] : [];
                $query->rows    = $data;
                $query->numRows = $i;

                return $query;
            } else {
                return true;
            }
        } else {
            throw new Exception($this->link->error, $this->link->errno);
        }
    }

    /**
     * Returns number of rows.
     *
     * @param string $sql
     *
     * @return int
     *
     * @throw Exception
     */
    public function count($sql)
    {
        $result = $this->link->query($sql);

        if (!$this->link->errno) {

            $row = $result->fetch_assoc();

            if (isset($row['COUNT(*)'])) {
                return $row['COUNT(*)'];
            }

            return $result->num_rows;
        } else {
            throw new Exception($this->link->error, $this->link->errno);
        }
    }

    /**
     * Sanitize SQL string.
     *
     * @param string $sql
     *
     * @return string
     */
    public function escape($sql)
    {
        return $this->link->real_escape_string($sql);
    }

    /**
     * Returns number of affected rows.
     *
     * @return int
     */
    public function affectedRows()
    {
        return $this->link->affected_rows;
    }

    /**
     * Returns ID of last inserted row.
     *
     * @return int
     */
    public function getLastId()
    {
        return $this->link->insert_id;
    }

    /**
     * Starts DB transaction.
     *
     * @return bool
     */
    public function startTransaction()
    {
        $this->link->query('set autocommit = 0');
        $this->link->query('start transaction');
    }

    /**
     * Commits DB transaction.
     *
     * @return bool
     */
    public function commit()
    {
        $this->link->query('commit');
        $this->link->query('set autocommit = 1');
    }

    /**
     * Rollback DB transaction.
     *
     * @return bool
     */
    public function rollback()
    {
        $this->link->query('rollback');
        $this->link->query('set autocommit = 1');
    }

    /**
     * Prepares SQL statement.
     *
     * @param string $sql
     *
     * @return mysqli_stmt
     */
    public function prepare($sql)
    {
        return $this->link->prepare($sql);
    }

    /**
     * Selects the default database for database queries.
     *
     * @param string $dbname
     *
     * @return bool
     */
    public function database($dbname)
    {
        return $this->link->select_db($dbname);
    }

    /**
     * Close database connection.
     *
     * @return void
     */
    public function __destruct()
    {
        $this->link->close();
    }
}
