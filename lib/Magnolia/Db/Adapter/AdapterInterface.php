<?php
/*
 * This file is part of the Magnolia library.
 *
 * (c) Mgnl Team <mgnl.library@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Magnolia\Db\Adapter;

/**
 * The database adapter interface
 */
interface AdapterInterface
{

    /**
     * Sets the current database
     *
     * @param string $dbname
     *
     * @return bool
     */
    public function database($dbname);

    /**
     * Executes the SQL query
     *
     * @param string $sql
     * @param bool   $asStatement
     * @param bool   $unbuffer
     *
     * @return mixed
     */
    public function query(string $sql, bool $asStatement = false, bool $unbuffer = false);

    /**
     * Sanitize value passed into SQL string
     *
     * @param string $sql
     *
     * @return string
     */
    public function escape($sql);

    /**
     * Returns number of affected rows.
     *
     * @return int
     */
    public function affectedRows();

    /**
     * Returns ID of added row.
     *
     * @return int
     */
    public function getLastId();

    /**
     * Sanitize value passed into SQL and adds quotation marks if needed.
     *
     * @param string|array $data
     * @param boolean      $quote
     *
     * @return string|array
     */
    public function sanitize($data, $quote = true);

    /**
     * Sanitize fields and tables names passed into SQL and adds quotation marks if needed.
     *
     * @param string|array $data
     * @param boolean      $useKeys
     *
     * @return string|array
     */
    public function fieldize($data, $useKeys = true);
}
