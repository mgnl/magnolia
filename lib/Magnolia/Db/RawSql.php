<?php
namespace Magnolia\Db;

class RawSql
{

    /**
     * Raw SQL.
     *
     * @var string
     */
    public $sql = '';

    /**
     * Sets raw SQL.
     *
     * @param string $sql
     * @return void
     */
    public function __construct($sql)
    {
        $this->sql = $sql;
    }

    /**
     * Returns raw SQL.
     *
     * @return string
     */
    public function __toString()
    {
        return $this->sql;
    }
}
