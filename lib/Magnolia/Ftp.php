<?php
namespace Magnolia;

use Exception;

class Ftp
{

    /**
     * FTP connections handler.
     *
     * @var Resources
     */
    protected $stream = null;

    /**
     * sFTP connection flag.
     *
     * @var bool
     */
    protected $ssl = false;

    /**
     * FTP host.
     *
     * @var string
     */
    protected $host = null;

    /**
     * FTP port.
     *
     * @var int
     */
    protected $port = 21;

    /**
     * FTP passive mode flag.
     *
     * @var bool
     */
    protected $pasv = false;

    /**
     * FTP connection timeout.
     *
     * @var int
     */
    protected $timeout = 90;

    /**
     * FTP username.
     *
     * @var string
     */
    protected $user = null;

    /**
     * FTP password.
     *
     * @var string
     */
    protected $password = null;

    /**
     * Sets FTP configuration.
     *
     * @param array $config
     *
     * @return $this
     */
    public function setConfig(array $config)
    {
        $this->ssl      = $this->config('ssl', $config, $this->ssl);
        $this->host     = $this->config('host', $config, $this->host);
        $this->port     = $this->config('port', $config, $this->port);
        $this->timeout  = $this->config('timeout', $config, $this->timeout);
        $this->pasv     = $this->config('passive_mode', $config, $this->pasv);
        $this->user     = $this->config('user', $config, $this->user);
        $this->password = $this->config('password', $config, $this->password);

        return $this;
    }

    /**
     * Opens an FTP or an Secure SSL-FTP connection. It depends on configuration.
     *
     * @param bool $login
     *
     * @return $this
     *
     * @throws Exception
     */
    public function connect()
    {
        $this->validateConfig();

        if ($this->ssl) {
            $this->stream = ftp_ssl_connect($this->host, $this->port, $this->timeout);
        } else {
            $this->stream = ftp_connect($this->host, $this->port, $this->timeout);
        }

        if (!$this->stream) {
            throw new Exception('Unable to connect to the FTP server.');
        }

        return $this;
    }

    /**
     * Login to FTP server using configuration.
     *
     * @return bool
     *
     * @throws Exception
     */
    public function login()
    {
        $this->validateConfig();

        $result = @ ftp_login($this->stream, $this->user, $this->password);

        if (!$result) {
            throw new Exception('Unable to login to the FTP server.');
        }

        return $this;
    }

    /**
     * Turns passive mode on or off. It depends on configuration.
     *
     * @return $this
     *
     * @throws Exception
     */
    public function pasv()
    {
        if (!ftp_pasv($this->stream, $this->pasv)) {
            throw new Exception('Unable to set passive mode flag.');
        }

        return $this;
    }

    /**
     * Returns the current directory name.
     *
     * @return string
     */
    public function pwd()
    {
        return @ ftp_pwd($this->stream);
    }

    /**
     * Changes the current directory on a FTP server.
     *
     * @param string $directory
     *
     * @return bool
     */
    public function chdir(string $directory)
    {
        return @ ftp_chdir($this->stream, $directory);
    }

    /**
     * Creates a directory.
     *
     * @param string $directory
     *
     * @return string|bool
     */
    public function mkdir(string $directory)
    {
        return @ ftp_mkdir($this->stream, $directory);
    }

    /**
     * Creates a directory recursively.
     *
     * @param string $directory
     * @param string $base
     *
     * @return bool
     */
    public function mkdirR(string $directory, string $base = null)
    {
        if (null !== $base) {
            if (!$this->chdir($base)) {
                return false;
            }
        }

        $directories = explode('/', $directory);

        $cwd = $this->pwd();

        foreach ($directories as $dir) {
            if (!$this->chdir($dir)) {
                $this->mkdir($dir);
                if (!$this->chdir($dir)) {
                    return false;
                }
            }
        }

        return $this->chdir($cwd);
    }

    /**
     * Removes a directory.
     *
     * @param string $directory
     *
     * @return bool
     */
    public function rmdir(string $directory)
    {
        return @ ftp_rmdir($this->stream, $directory);
    }

    /**
     * Returns a list of files in the given directory.
     *
     * @param string $directory
     *
     * @return array|bool
     */
    public function nlist(string $directory)
    {
        return ftp_nlist($this->stream, $directory);
    }

    /**
     * Checks whether the directory exists on the FTP server.
     *
     * @param string $directory
     *
     * @return bool
     */
    public function isDir(string $directory)
    {
        $pwd    = $this->pwd();
        $result = $this->chdir($directory);
        $this->chdir($pwd);

        return $result;
    }

    /**
     * Uploads a file to the FTP server.
     *
     * @param string $remoteFile
     * @param string $localFile
     * @param int $mode Must be either FTP_ASCII or FTP_BINARY
     *
     * @return bool
     */
    public function put(string $remoteFile, string $localFile, $mode = FTP_BINARY)
    {
        return @ ftp_put($this->stream, $remoteFile, $localFile, $mode);
    }

    /**
     * Renames a file or a directory on the FTP server.
     *
     * @param string $oldname
     * @param string $newname
     *
     * @return bool
     */
    public function rename(string $oldname, string $newname)
    {
        return @ ftp_rename($this->stream, $oldname, $newname);
    }

    /**
     * Deletes a file on the FTP server.
     *
     * @param string $path
     *
     * @return bool
     */
    public function delete(string $path)
    {
        return @ ftp_delete($this->stream, $path);
    }

    /**
     * Removes files and directories recursively on the FTP server.
     *
     * @param string $path
     *
     * @return bool
     */
    public function removeR(string $path)
    {
        if (!($this->rmdir($path) || $this->delete($path))) {

            $files = $this->nlist($path);

            if (!empty($files)) {
                foreach ($files as $file) {
                    if (in_array(basename($file), ['.', '..'])) {
                        continue;
                    }

                    if (!$this->removeR($file)) {
                        return false;
                    }
                }
            }

            if (!$this->rmdir($path)) {
                return false;
            }
        }

        return true;
    }

    /**
     * Closes an FTP connection.
     *
     * @return bool
     */
    public function close()
    {
        return ftp_close($this->stream);
    }

    /**
     * Validates the FTP configuration.
     *
     * @return void
     *
     * @throws Exception
     */
    protected function validateConfig()
    {
        if (!$this->host) {
            throw new Exception('There is no Ftp::host parameter.');
        }

        if (!$this->user) {
            throw new Exception('There is no Ftp::user parameter.');
        }

        if (!$this->password) {
            throw new Exception('There is no Ftp::password parameter.');
        }
    }

    /**
     * Uploads a file(s) on the FTP server.
     *
     * @param array|string $files
     * @param string $remoteFile
     *
     * @return bool
     */
    public function upload($files, $remoteFile = null)
    {
        if (!is_array($files)) {

            if (!$this->put($remoteFile, $files)) {

                if ($this->mkdirR(dirname($remoteFile))) {
                    return $this->put($remoteFile, $files);
                }

                return false;
            }

            return true;
        }

        foreach ($files as $source => $destination) {

            if (is_dir($source)) {
                if (!$this->mkdir($destination)) {
                    if (!$this->mkdirR($destination)) {
                        return false;
                    }
                }
            } else {

                if (!$this->put($destination, $source)) {

                    if (!$this->mkdirR(dirname($destination))) {
                        return false;
                    }

                    if (!$this->put($destination, $source)) {
                        return false;
                    }
                }
            }
        }

        return true;
    }

    /**
     * Returns configuration parameter or $default value.
     *
     * @param string $name
     * @param array $config
     * @param mixed $default
     *
     * @return mixed
     */
    private function config($name, array $config, $default = null)
    {
        return isset($config[$name]) ? $config[$name] : $default;
    }
}
