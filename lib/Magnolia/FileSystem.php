<?php
namespace Magnolia;

use ZipArchive;

class FileSystem
{

    /**
     * Removes directory recursively.
     *
     * @param string $dir
     * @param bool $withIt
     * @return void
     */
    public static function rmdirRecursive($dir, $withIt = false)
    {
        if (is_dir($dir)) {

            $items = array_diff(scandir($dir), ['..', '.']);

            foreach ($items as $item) {

                $object = $dir.'/'.$item;

                if ('dir' === filetype($object)) {
                    if (!self::rmdirRecursive($dir.'/'.$item)) {
                        return false;
                    }
                    if (!rmdir($dir.'/'.$item)) {
                        return false;
                    }
                } else {
                    if (!unlink($object)) {
                        return false;
                    }
                }
            }

            if ($withIt) {
                return rmdir($dir);
            }

            return true;
        }

        return false;
    }

    /**
     * Unzip the archive into the passed directory.
     *
     * @param string $filepath
     * @param string $destination
     * @return bool
     */
    public static function unzip($filepath, $destination)
    {
        $zip = new ZipArchive();

        #TODO: validate filepath.
        if (!$zip->open($filepath)) {
            return false;
        }

        if (!$zip->extractTo($destination)) {
            return false;
        }

        return $zip->close();
    }

    /**
     * Creates list of files and directories contained in given path.
     *
     * @param string $path
     * @param array $result
     * @return void
     */
    public static function itemsList($path, array &$result)
    {
        $items = array_diff(scandir($path), ['..', '.']);

        foreach ($items as $item) {

            $fullPath = $path.'/'.$item;

            if (is_dir($fullPath)) {
                $result[] = $fullPath;
                self::itemsList($fullPath, $result);
            } else {
                $result[] = $fullPath;
            }
        }
    }

    /**
     * Creates a directory with the full path to it.
     *
     * @param string $path
     * @return bool
     */
    public static function createRecursively($base, $path)
    {
        $cwd = getcwd();

        chdir($base);

        $dirs = explode('/', $path);

        foreach ($dirs as $dir) {
            if (!file_exists($dir)) {
                mkdir($dir);
            }
            chdir($dir);
        }

        chdir($cwd);

        return true;
    }
}
