<?php
namespace Magnolia;

use Exception;

class Validator
{

    use Validator\TAllowEmpty;
    use Validator\TBetween;
    use Validator\TCountry;
    use Validator\TDate;
    use Validator\TEachIn;
    use Validator\TEachNotIn;
    use Validator\TEnum;
    use Validator\TExists;
    use Validator\TForbbiden;
    use Validator\TForbbidenIf;
    use Validator\TLength;
    use Validator\TLessThan;
    use Validator\TMoreThan;
    use Validator\TReadOnly;
    use Validator\TRegex;
    use Validator\TRequired;
    use Validator\TRequiredIf;
    use Validator\TMessageType;
    use Validator\TType;
    use Validator\TUnique;

    /**
     * Data to validate.
     *
     * @vara array
     */
    protected $data = [];

    /**
     * Field currently validated.
     *
     * @var string
     */
    protected $field = null;

    /**
     * Validation parameters
     *
     * @var array
     */
    protected $toValidate = [];

    /**
     * Collection of errors.
     *
     * @vara array
     */
    protected $errors = [];

    /**
     * Collection of warnings.
     *
     * @var array
     */
    protected $warnings = [];

    /**
     * Current field is required?
     *
     * @var bool
     */
    private $fieldRequired = false;

    /**
     * Current field is set?
     *
     * @var bool
     */
    private $fieldIsset = false;

    /**
     * If field is invalid the message will be added to the specified array.
     *
     * @var enum error|warning
     */
    private $messageType = 'error';

    /**
     * Sets data to validate.
     *
     * @param array $data
     *
     * @return $this
     */
    public function setData($data)
    {
        $this->data            = $data;
        $this->field           = null;
        $this->toValidate      = [];
        $this->errors          = [];
        $this->currentRequired = false;
        $this->messageType     = 'error';
        $this->allowEmpty      = false;

        return $this;
    }

    /**
     * Returns validation errors.
     *
     * @return array
     */
    public function getErrors()
    {
        return $this->errors;
    }

    /**
     * Returns validation warnings.
     *
     * @return array
     */
    public function getWarnings()
    {
        return $this->warnings;
    }

    /**
     * Sets the field is currently validated.
     *
     * @param string $field
     *
     * @return $this
     */
    public function setField($field = null)
    {
        $this->field = $field;

        return $this;
    }

    /**
     * @return $this
     */
    public function validate()
    {
        foreach ($this->toValidate as $field => $params) {

            $this->messageType   = $params['messageType'] ?? 'errors';
            $this->fieldRequired = $params['required'] ?? false;
            $this->fieldIsset    = array_key_exists($field, $this->data);

            if ($this->fieldIsset && isset($params['allowEmpty']) && $params['allowEmpty']) {
                if (empty($this->data[$field]) && !is_numeric($this->data[$field])) {
                    continue;
                }
            }

            foreach (array_keys($params) as $param) {

                $method = 'validate'.ucfirst($param);

                if (method_exists($this, $method)) {
                    call_user_func_array([$this, $method], [$field]);
                }
            }
        }

        return $this;
    }

    /**
     * Checks whether the field for validation is set and returns its name.
     *
     * @return string
     *
     * @throws Exception
     */
    protected function field()
    {
        if (null === $this->field) {
            throw new Exception('You need to set field first.');
        }

        return $this->field;
    }
}
