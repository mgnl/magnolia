<?php

namespace Magnolia\Validator;

trait TReadOnly
{

    public function readOnly($previous = null)
    {
        $field = $this->field();

        $this->toValidate[$field]['readOnly'] = $previous;

        return $this;
    }

    protected function validateReadOnly($field)
    {
        if (array_key_exists($field, $this->data)) {

            if ($this->data[$field] !== $this->toValidate[$field]['readOnly']) {

                $type = $this->messageType;

                $this->{$type}[$field] = "The '{$field}' is read only.";
            }
        }
    }
}
