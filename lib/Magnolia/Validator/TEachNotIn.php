<?php
namespace Magnolia\Validator;

trait TEachNotIn
{

    public function eachNotIn($array)
    {
        $field = $this->field();

        $this->toValidate[$field]['eachNotIn'] = $array;

        return $this;
    }

    protected function validateEachNotIn($field)
    {
        if (!$this->fieldIsset) {
            return;
        }

        $values = (array) $this->data[$field];

        $type = $this->messageType;

        $invalid = [];
        foreach ($values as $value) {
            if (in_array($value, $this->toValidate[$field]['eachNotIn'])) {
                $invalid[] = $value;
            }
        }

        if ($invalid) {
            $in      = implode('|', $this->toValidate[$field]['eachNotIn']);
            $invalid = implode(',', $invalid);

            $this->{$type}[$field] = "The '{$field}' value(s) [{$invalid}] must not to be in: [{$in}]";
        }
    }
}
