<?php
namespace Magnolia\Validator;

trait TForbbidenIf
{

    public function forbbidenIf($fields, $values)
    {
        $field = $this->field();

        $this->toValidate[$field]['forbbidenIf'] = [$fields, $values];

        return $this;
    }

    protected function validateForbbidenIf($field)
    {
        $condition = $this->toValidate[$field]['forbbidenIf'];

        if (0 === strpos($condition[0], '@')) {
            $cField = $this->data[substr($condition[0], 1)] ?? null;
        } else {
            $cField = $condition[0];
        }

        $cValue = $condition[1] ?? null;

        if ($this->fieldIsset && ($cValue === $cField)) {

            $type = $this->messageType;

            $this->{$type}[$field] = "The '{$field}' is forbbiden";

            if ($condition[0] && $cValue) {
                $this->{$type}[$field] .= " when '{$condition[0]}' = '{$cValue}'";
            }
        }
    }
}
