<?php
namespace Magnolia\Validator;

trait TDate
{

    /**
     * Allowed formats: YYYY-MM-DD, YYYY-MM-DD HH:MM, YYYY-MM-DD HH:MM:SS
     *
     * @return $this
     */
    public function date()
    {
        $field = $this->field();

        $this->toValidate[$field]['date'] = true;

        return $this;
    }

    protected function validateDate($field)
    {
        if (!$this->fieldIsset) {
            return;
        }

        $datetime = explode(' ', $this->data[$field], 2);

        $date = $datetime[0] ?? null;
        $time = $datetime[1] ?? null;

        $validDate = false;
        if ($date) {
            $validDate = $this->_validateDate($date);
        }

        $validTime = true;
        if ($time) {
            $validTime = $this->_validateTime($time);
        }

        $type = $this->messageType;

        if (!$validDate) {
            $this->{$type}[$field] = "The '{$field}' has incorrect date: '{$date}'";
        }

        if (!$validTime) {
            $this->{$type}[$field] = "The '{$field}' has incorrect time: '{$time}'";
        }

        if (!$validDate && !$validTime) {
            $this->{$type}[$field] = "The '{$field}' has incorrect datetime: '{$this->data[$field]}'";
        }
    }

    private function _validateDate($date)
    {
        $dateParts = explode('-', $date);

        $year  = $dateParts[0] ?? null;
        $month = $dateParts[1] ?? null;
        $day   = $dateParts[2] ?? null;

        return checkdate($month, $day, $year);
    }

    private function _validateTime($time)
    {
        $bits    = explode(':', $time);
        $hours   = $bits[0] ?? null;
        $minutes = $bits[1] ?? null;
        $seconds = $bits[2] ?? null;

        if (null === $hours || ($hours < 0 || $hours > 23)) {
            return false;
        }

        if (null === $minutes || ($minutes < 0 || $minutes > 59)) {
            return false;
        }

        if (null !== $seconds && ($seconds < 0 || $seconds > 59)) {
            return false;
        }

        return true;
    }
}
