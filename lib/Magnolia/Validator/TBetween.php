<?php
namespace Magnolia\Validator;

trait TBetween
{

    public function between($min, $max)
    {
        $field = $this->field();

        $this->toValidate[$field]['between'] = [$min, $max];

        return $this;
    }

    /**
     * @param string $field
     *
     * @return void
     */
    protected function validateBetween($field)
    {
        if (!$this->fieldIsset) {
            return;
        }

        $between = $this->toValidate[$field]['between'];

        $type = $this->messageType;

        if ($this->data[$field] < $between[0] || $this->data[$field] > $between[1]) {
            $this->{$type}[$field] = "Value of '{$field}' should be between ";
            $this->{$type}[$field] .= "'{$between[0]}' and {$between[1]}.";
        }
    }
}
