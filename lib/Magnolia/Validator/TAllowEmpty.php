<?php
namespace Magnolia\Validator;

trait TAllowEmpty
{

    public function allowEmpty($allow = true)
    {
        $field = $this->field();

        $this->toValidate[$field]['allowEmpty'] = $allow;

        return $this;
    }

    /**
     * @param string $field
     *
     * @return void
     */
    protected function validateAllowEmpty($field)
    {
        if (!$this->fieldIsset) {
            return;
        }

        $allow = $this->toValidate[$field]['allowEmpty'];

        $type = $this->messageType;

        if (!$allow && empty($this->data[$field]) && !is_numeric($this->data[$field])) {
            $this->{$type}[$field] = "The '{$field}' can not be empty";
        }
    }
}
