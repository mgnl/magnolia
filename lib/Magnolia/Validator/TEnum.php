<?php
namespace Magnolia\Validator;

trait TEnum
{

    public function enum($spec)
    {
        $field = $this->field();

        $this->toValidate[$field]['enum'] = $spec;

        return $this;
    }

    protected function validateEnum($field)
    {
        if (!$this->fieldIsset) {
            return;
        }

        $enums = $this->toValidate[$field]['enum'];

        $value = $this->data[$field] ?? null;

        if (!in_array($value, $enums)) {

            $values = implode('|', $enums);

            $type = $this->messageType;

            $this->{$type}[$field] = "The '{$field}' expects to be in [{$values}], ";
            $this->{$type}[$field] .= "'{$value}' given.";
        }
    }
}
