<?php
namespace Magnolia\Validator;

trait TType
{

    public function type($type)
    {
        $field = $this->field();

        $this->toValidate[$field]['type'] = $type;

        return $this;
    }

    /**
     * @param string $field
     *
     * @return void
     */
    protected function validateType($field)
    {
        if (!$this->fieldIsset) {
            return;
        }

        $type      = $this->toValidate[$field]['type'];
        $fieldType = gettype($this->data[$field]);
        $msgType   = $this->messageType;

        if ('integer' == $fieldType && in_array($type, ['double', 'int'], true)) {
            return;
        }

        if ($type != $fieldType && !$this->checkString($this->data[$field], $type)) {
            $this->{$msgType}[$field] = "The '{$field}' must be of type {$type}, {$fieldType} given";
        }
    }

    private function checkString($var, $type)
    {
        if ('double' == $type) {
            $v = (float) $var;
            $v = (string) $v;
            if ($v === $var) {
                return true;
            }
        } elseif ('integer' == $type || 'int' == $type) {
            $v = (int) $var;
            $v = (string) $v;
            if ($v === $var) {
                return true;
            }
        } elseif ('boolean' == $type) {
            if (in_array($var, [false, true, 0, 1, '0', '1'], true)) {
                return true;
            }
        }

        return false;
    }
}
