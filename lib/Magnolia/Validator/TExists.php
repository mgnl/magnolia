<?php
namespace Magnolia\Validator;

/* #TODO: to impement */

trait TExists
{

    public function exists($spec)
    {
        $field = $this->field();

        $this->toValidate[$field]['exists'] = $spec;

        return $this;
    }

    protected function validateExists($field)
    {
        if (!$this->fieldIsset) {
            return;
        }

        $spec = $this->toValidate[$field]['exists'];

        $type = $this->messageType;

        if (is_array($spec) && !in_array($this->data[$field], $spec)) {

            $values = implode('|', $spec);

            $this->{$type}[$field] = "The '{$field}' expects to be in [{$values}], ";
            $this->{$type}[$field] .= "'{$this->data[$field]}' given.";
        }

        if (is_callable($spec)) {
            if (!call_user_func($spec, $this->data[$field])) {
                $this->{$type}[$field] = "The '{$field}' of {$this->data[$field]} does not exist.";
            }
        }
    }
}
