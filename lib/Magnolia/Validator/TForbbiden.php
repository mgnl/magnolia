<?php
namespace Magnolia\Validator;

trait TForbbiden
{

    public function forbbiden($forbbiden = true)
    {
        $field = $this->field();

        $this->toValidate[$field]['forbbiden'] = $forbbiden;

        return $this;
    }

    protected function validateForbbiden($field)
    {
        if (array_key_exists($field, $this->data)) {

            $type = $this->messageType;

            $this->{$type}[$field] = "The '{$field}' is forbbiden.";
        }
    }
}
