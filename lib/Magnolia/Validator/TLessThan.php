<?php
namespace Magnolia\Validator;

trait TLessThan
{

    public function lessThan($toCompare)
    {
        $field = $this->field();

        $this->toValidate[$field]['lessThan'] = $toCompare;

        return $this;
    }

    protected function validateLessThan($field)
    {
        if (!$this->fieldIsset) {
            return;
        }

        $toCompare = $this->toValidate[$field]['lessThan'];

        if ($this->data[$field] >= $toCompare) {

            $type = $this->messageType;

            $this->{$type}[$field] = "Value of '{$field}' should be less than '{$toCompare}'.";
        }
    }
}
