<?php
namespace Magnolia\Validator;

trait TRegex
{

    public function regex($spec)
    {
        $field = $this->field();

        $this->toValidate[$field]['regex'] = $spec;

        return $this;
    }

    protected function validateRegex($field)
    {
        if (!$this->fieldIsset) {
            return;
        }

        $regex = $this->toValidate[$field]['regex'];

        $value = $this->data[$field] ?? null;

        if (!preg_match($value, $regex)) {

            $type = $this->messageType;

            $this->{$type}[$field] = "The '{$field}' expects to be valid regex: ";
            $this->{$type}[$field] .= $regex;
        }
    }
}
