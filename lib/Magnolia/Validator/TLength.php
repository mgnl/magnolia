<?php
namespace Magnolia\Validator;

trait TLength
{

    public function length($min = null, $max = null)
    {
        $field = $this->field();

        $this->toValidate[$field]['length'] = [$min, $max];

        return $this;
    }

    protected function validateLength($field)
    {
        if (!$this->fieldIsset) {
            return;
        }

        $length = mb_strlen($this->data[$field]);
        $min    = $this->toValidate[$field]['length'][0] ?? null;
        $max    = $this->toValidate[$field]['length'][1] ?? null;

        $type = $this->messageType;

        if (null === $max) {
            if ($length != $min) {
                $this->{$type}[$field] = "The length of '{$field}' should be {$min}";
            }
        } elseif ($length < $min || $length > $max) {
            $this->{$type}[$field] = "The length of '{$field}' should be between {$min} and {$max}";
        }
    }
}
