<?php
namespace Magnolia\Validator;

trait TRequired
{

    public function required($required = true)
    {
        $field = $this->field();

        $this->toValidate[$field]['required'] = $required;

        return $this;
    }

    protected function validateRequired($field)
    {
        if (!$this->fieldIsset) {

            $type = $this->messageType;

            $this->{$type}[$field] = "The '{$field}' is required.";
        }
    }
}
