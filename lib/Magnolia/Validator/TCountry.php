<?php
namespace Magnolia\Validator;

use Exception;

trait TCountry
{

    private static $iso = [
        'ISO 3166-1 alfa-2' => [
            'af', // Afghanistan
            'al', // Albania
            'dz', // Algeria
            'ad', // Andorra
            'ao', // Angola
            'ai', // Anguilla
            'aq', // Antarctica
            'ag', // Antigua and Barbuda
            'sa', // Saudi Arabia
            'ar', // Argentina
            'am', // Armenia
            'aw', // Aruba
            'au', // Australia
            'at', // Austria
            'az', // Azerbaijan
            'bs', // Bahamas
            'bh', // Bahrain
            'bd', // Bangladesh
            'bb', // Barbados
            'be', // Belgium
            'bz', // Belize
            'bj', // Benin
            'bm', // Bermuda
            'bt', // Bhutan
            'by', // Belarus
            'bo', // Bolivia, Plurinational State of
            'bq', // Bonaire, Sint Eustatius and Saba
            'ba', // Bosnia and Herzegovina
            'bw', // Botswana
            'br', // Brazil
            'bn', // Brunei Darussalam
            'io', // British Indian Ocean Territory
            'vg', // Virgin Islands, British
            'bg', // Bulgaria
            'bf', // Burkina Faso
            'bi', // Burundi
            'cl', // Chile
            'cn', // China
            'hr', // Croatia
            'cw', // Curaçao
            'cy', // Cyprus
            'td', // Chad
            'me', // Montenegro
            'cz', // Czechia
            'um', // United States Minor Outlying Islands
            'dk', // Denmark
            'cd', // Congo, the Democratic Republic of the
            'dm', // Dominica
            'do', // Dominican Republic
            'dj', // Djibouti
            'eg', // Egypt
            'ec', // Ecuador
            'er', // Eritrea
            'ee', // Estonia
            'et', // Ethiopia
            'fk', // Falkland Islands (Malvinas)
            'fj', // Fiji
            'ph', // Philippines
            'fi', // Finland
            'fr', // France
            'tf', // French Southern Territories
            'ga', // Gabon
            'gm', // Gambia
            'gs', // South Georgia and the South Sandwich Islands
            'gh', // Ghana
            'gi', // Gibraltar
            'gr', // Greece
            'gd', // Grenada
            'gl', // Greenland
            'ge', // Georgia
            'gu', // Guam
            'gg', // Guernsey
            'gf', // French Guiana
            'gy', // Guyana
            'gp', // Guadeloupe
            'gt', // Guatemala
            'gw', // Guinea-Bissau
            'gq', // Equatorial Guinea
            'gn', // Guinea
            'ht', // Haiti
            'es', // Spain
            'nl', // Netherlands
            'hn', // Honduras
            'hk', // Hong Kong
            'in', // India
            'id', // Indonesia
            'iq', // Iraq
            'ir', // Iran, Islamic Republic of
            'ie', // Ireland
            'is', // Iceland
            'il', // Israel
            'jm', // Jamaica
            'jp', // Japan
            'ye', // Yemen
            'je', // Jersey
            'jo', // Jordan
            'ky', // Cayman Islands
            'kh', // Cambodia
            'cm', // Cameroon
            'ca', // Canada
            'qa', // Qatar
            'kz', // Kazakhstan
            'ke', // Kenya
            'kg', // Kyrgyzstan
            'ki', // Kiribati
            'co', // Colombia
            'km', // Comoros
            'cg', // Congo
            'kr', // Korea, Republic of
            'kp', // Korea, Democratic People's Republic of
            'cr', // Costa Rica
            'cu', // Cuba
            'kw', // Kuwait
            'la', // Lao People's Democratic Republic
            'ls', // Lesotho
            'lb', // Lebanon
            'lr', // Liberia
            'ly', // Libyan Arab Jamahiriya
            'li', // Liechtenstein
            'lt', // Lithuania
            'lu', // Luxembourg
            'lv', // Latvia
            'mk', // Macedonia, the former Yugoslav Republic of
            'mg', // Madagascar
            'yt', // Mayotte
            'mo', // Macao
            'mw', // Malawi
            'mv', // Maldives
            'my', // Malaysia
            'ml', // Mali
            'mt', // Malta
            'mp', // Northern Mariana Islands
            'ma', // Morocco
            'mq', // Martinique
            'mr', // Mauritania
            'mu', // Mauritius
            'mx', // Mexico
            'fm', // Micronesia, Federated States of
            'mm', // Myanmar
            'md', // Moldova, Republic of
            'mc', // Monaco
            'mn', // Mongolia
            'ms', // Montserrat
            'mz', // Mozambique
            'na', // Namibia
            'nr', // Nauru
            'np', // Nepal
            'de', // Germany
            'ne', // Niger
            'ng', // Nigeria
            'ni', // Nicaragua
            'nu', // Niue
            'nf', // Norfolk Island
            'no', // Norway
            'nc', // New Caledonia
            'nz', // New Zealand
            'om', // Oman
            'pk', // Pakistan
            'pw', // Palau
            'ps', // Palestinian Territory, Occupied
            'pa', // Panama
            'pg', // Papua New Guinea
            'py', // Paraguay
            'pe', // Peru
            'pn', // Pitcairn
            'pf', // French Polynesia
            'pl', // Poland
            'pr', // Puerto Rico
            'pt', // Portugal
            'za', // South Africa
            'cf', // Central African Republic
            'cv', // Cape Verde
            're', // Réunion
            'ru', // Russian Federation
            'ro', // Romania
            'rw', // Rwanda
            'eh', // Western Sahara
            'kn', // Saint Kitts and Nevis
            'lc', // Saint Lucia
            'vc', // Saint Vincent and the Grenadines
            'bl', // Saint Barthélemy
            'mf', // Saint Martin (French part)
            'pm', // Saint Pierre and Miquelon
            'sv', // El Salvador
            'as', // American Samoa
            'ws', // Samoa
            'sm', // San Marino
            'sn', // Senegal
            'rs', // Serbia
            'sc', // Seychelles
            'sl', // Sierra Leone
            'sg', // Singapore
            'sx', // Sint Maarten (Dutch part)
            'sk', // Slovakia
            'si', // Slovenia
            'so', // Somalia
            'lk', // Sri Lanka
            'us', // United States
            'sz', // Swaziland
            'sd', // Sudan
            'ss', // South Sudan
            'sr', // Suriname
            'sj', // Svalbard and Jan Mayen
            'sy', // Syrian Arab Republic
            'ch', // Switzerland
            'se', // Sweden
            'tj', // Tajikistan
            'th', // Thailand
            'tw', // Taiwan, Province of China
            'tz', // Tanzania, United Republic of
            'tl', // Timor-Leste
            'tg', // Togo
            'tk', // Tokelau
            'to', // Tonga
            'tt', // Trinidad and Tobago
            'tn', // Tunisia
            'tr', // Turkey
            'tm', // Turkmenistan
            'tc', // Turks and Caicos Islands
            'tv', // Tuvalu
            'ug', // Uganda
            'ua', // Ukraine
            'uy', // Uruguay
            'uz', // Uzbekistan
            'vu', // Vanuatu
            'wf', // Wallis and Futuna
            'va', // Holy See (Vatican City State)
            've', // Venezuela, Bolivarian Republic of
            'hu', // Hungary
            'gb', // United Kingdom
            'vn', // Viet Nam
            'it', // Italy
            'ci', // Côte d'Ivoire
            'bv', // Bouvet Island
            'cx', // Christmas Island
            'im', // Isle of Man
            'sh', // Saint Helena, Ascension and Tristan da Cunha
            'ax', // Åland Islands
            'ck', // Cook Islands
            'vi', // Virgin Islands, U.S.
            'hm', // Heard Island and McDonald Islands
            'cc', // Cocos (Keeling) Islands
            'mh', // Marshall Islands
            'fo', // Faroe Islands
            'sb', // Solomon Islands
            'st', // Sao Tome and Principe
            'zm', // Zambia
            'zw', // Zimbabwe
            'ae', // United Arab Emirates
        ],
    ];

    /**
     * @param array|string $_ Default is ISO 3166-1 alfa-2
     *
     * @return $this
     */
    public function country()
    {
        $field = $this->field();

        $this->toValidate[$field]['country'] = $this->getSpec(func_get_args());

        return $this;
    }

    /**
     * @param array|string $_ Default is ISO 3166-1 alfa-2
     *
     * @return $this
     */
    public function countries()
    {
        $field = $this->field();

        $this->toValidate[$field]['countries'] = $this->getSpec(func_get_args());

        return $this;
    }

    protected function validateCountry($field)
    {
        if (!$this->fieldIsset) {
            return;
        }

        $value = $this->data[$field];

        $type = $this->messageType;

        $countries = $this->toValidate[$field]['country'];

        if (!in_array($value, $countries, true)) {
            $this->{$type}[$field] = "The '{$field}' value '{$value}' is invalid";
        }
    }

    protected function validateCountries($field)
    {
        if (!$this->fieldIsset) {
            return;
        }

        $values = $this->data[$field];

        $type = $this->messageType;

        $invalid = [];

        $countries = $this->toValidate[$field]['countries'];

        foreach ($values as $value) {
            if (!in_array($value, $countries)) {
                $invalid[] = $value;
            }
        }

        if ($invalid) {
            $invalid = implode(',', $invalid);

            $this->{$type}[$field] = "The '{$field}' value(s) [{$invalid}] are invalid";
        }
    }

    private function getSpec($args)
    {
        if (!$args) {
            return self::$iso['ISO 3166-1 alfa-2'];
        }

        $retval = [];

        foreach ($args as $arg) {
            if (!is_array($arg)) {
                if ('ISO' == $arg) {
                    foreach (self::$iso as $iso) {
                        $retval = array_merge($retval, $iso);
                    }
                } elseif (isset(self::$iso[$arg])) {
                    $retval = array_merge($retval, self::$iso[$arg]);
                } else {
                    throw new Exception("There is no validation rule '{$arg}' for country");
                }
            } else {
                $retval = array_merge($arg, $retval);
            }
        }

        return $retval;
    }
}
