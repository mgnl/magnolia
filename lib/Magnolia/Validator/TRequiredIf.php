<?php

namespace Magnolia\Validator;

trait TRequiredIf
{

    public function requiredIf($fields, $values)
    {
        $field = $this->field();

        $this->toValidate[$field]['requiredIf'] = [$fields, $values];

        return $this;
    }

    protected function validateRequiredIf($field)
    {
        $condition = $this->toValidate[$field]['requiredIf'];

        if (0 === strpos($condition[0], '@')) {
            $cField = $this->data[substr($condition[0], 1)] ?? null;
        } else {
            $cField = $condition[0];
        }

        $cValue = $condition[1] ?? null;

        if (!$this->fieldIsset && ($cValue === $cField)) {

            $type = $this->messageType;

            $this->{$type}[$field] = "The '{$field}' is required";

            if ($condition[0] && $cValue) {
                $this->{$type}[$field] .= " when '{$condition[0]}' is '{$cValue}'";
            }
        }
    }
}
