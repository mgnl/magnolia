<?php
namespace Magnolia\Validator;

trait TMessageType
{

    public function messageType($type)
    {
        if (!in_array($type, ['error', 'warning'])) {
            throw new Exception("Invalid message type, expected [error|message], '{$type}' given.");
        }

        $field = $this->field();

        $this->toValidate[$field]['messageType'] = $type.'s';

        return $this;
    }
}
