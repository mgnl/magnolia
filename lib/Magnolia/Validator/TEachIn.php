<?php
namespace Magnolia\Validator;

trait TEachIn
{

    public function eachIn($array)
    {
        $field = $this->field();

        $this->toValidate[$field]['eachIn'] = $array;

        return $this;
    }

    protected function validateEachIn($field)
    {
        if (!$this->fieldIsset) {
            return;
        }

        $values = (array) $this->data[$field];

        $type = $this->messageType;

        $invalid = [];
        foreach ($values as $value) {
            if (!in_array($value, $this->toValidate[$field]['eachIn'])) {
                $invalid[] = $value;
            }
        }

        if ($invalid) {
            $in      = implode('|', $this->toValidate[$field]['eachIn']);
            $invalid = implode(',', $invalid);

            $this->{$type}[$field] = "The '{$field}' value(s) [{$invalid}] must to be in: [{$in}]";
        }
    }
}
