<?php
namespace Magnolia\Validator;

/* #TODO: to implement */

trait TUnique
{

    public function unique($spec)
    {
        $field = $this->field();

        $this->toValidate[$field]['unique'] = $spec;

        return $this;
    }

    protected function validateUnique($field)
    {
        if (!$this->fieldIsset) {
            return;
        }

        $spec = $this->toValidate[$field]['unique'];

        if (is_array($spec)) {
            $result = in_array($this->data[$field], $spec);
        } else {
            $result = call_user_func($spec, $this->data[$field]);
        }

        if (!empty($result)) {

            $type = $this->messageType;

            $this->{$type}[$field] = "Duplicate entry for '{$field}'.";
        }
    }
}
