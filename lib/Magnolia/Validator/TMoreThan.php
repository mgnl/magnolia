<?php
namespace Magnolia\Validator;

trait TMoreThan
{

    public function moreThan($toCompare)
    {
        $field = $this->field();

        $this->toValidate[$field]['moreThan'] = $toCompare;

        return $this;
    }

    protected function validateMoreThan($field)
    {
        if (!$this->fieldIsset) {
            return;
        }

        $toCompare = $this->toValidate[$field]['moreThan'];

        if ($this->data[$field] <= $toCompare) {

            $type = $this->messageType;

            $this->{$type}[$field] = "Value of '{$field}' should be less than '{$toCompare}'.";
        }
    }
}
