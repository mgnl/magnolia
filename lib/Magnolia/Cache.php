<?php

namespace Magnolia;

use Exception;

/**
 * For names like a!aa, a#aa, a%aa, a&aa, all of them will be normalize to a#aa.
 * Base chars range is [a-zA-Z0-9._], allowed special chars are . and _,
 * all the others will be replace by #
 */
class Cache
{
    /**
     * Route of cache files directory.
     *
     * @var string
     */
    protected $directory = '';

    /**
     * Sets cache files directory route.
     *
     * @param string $path Directory path.
     * @return void
     * @throws Exception
     */
    public function setDirectory($path)
    {
        $path = rtrim($path, '\\/');

        if (!file_exists($path)) {
            mkdir($path, 0777, true);
        }

        $this->directory = $path.'/';
    }

    /**
     * Set cache file.
     *
     * @param string $name File name.
     * @param mixed $value File data.
     * @param int $time Life time in seconds from now.
     * @return void
     */
    public function set($name, $value, $time)
    {
        $this->delete($name);
        $data   = serialize($value);
        $expire = time() + (int) $time;
        file_put_contents($this->directory.$this->normalizeName($name).'.'.$expire, $data);
    }

    /**
     * Get cache file data from $directory route.
     *
     * @param string $name File name.
     * @return void
     */
    public function get($name)
    {
        $fileList = glob($this->directory.$this->normalizeName($name).'.*');

        $time = time();

        foreach ($fileList as $fileName) {

            $explode = explode('.', $fileName);

            if ($time > end($explode)) {
                unlink($fileName);
            } else {
                return unserialize(file_get_contents($fileName));
            }
        }
    }

    /**
     * Delete cache file from $directory route.
     *
     * @param string $name File name
     *
     * @return void
     */
    public function delete($name)
    {
        $fileList = glob($this->directory.$this->normalizeName($name).'.*');

        foreach ($fileList as $fileName) {
            unlink($fileName);
        }
    }

    /**
     * Name converter to approachable state.
     *
     * @param string $name File name.
     * @return string $name Converted value.
     */
    final protected function normalizeName($name)
    {
        return preg_replace('/[^a-zA-Z0-9\.\_]/', '#', $name);
    }
}
